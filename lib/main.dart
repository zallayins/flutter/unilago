import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:unilago_prototype/routes.dart';
import 'package:unilago_prototype/states/cart_state.dart';
import 'package:unilago_prototype/states/conectivity_services.dart';
import 'package:unilago_prototype/states/login_state.dart';
import 'package:unilago_prototype/states/products_state.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(MyApp());
  });
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        StreamProvider<ConnectivityStatus>(
          create: (_) =>
              ConnectivityService().connectionStatusController.stream,
        ),
        ChangeNotifierProvider<LoginState>(
          create: (context) => LoginState(),
        ),
        ChangeNotifierProvider<ProductState>(
          create: (context) => ProductState(),
        ),
        ChangeNotifierProvider<CartState>(
          create: (context) => CartState(),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          primaryColor: Color(0xff444dff), //Color(0xff4D0084),
          accentColor: Color(0xFFBFBEBE),
          buttonColor: Color(0xff444dff),
          cardColor: Color(0xffFFE5E5),
          iconTheme: IconThemeData(
            color: Color(0xff852EAD),
          ),
          appBarTheme: AppBarTheme(
            elevation: 5.0,
            color: Color(0xff4D0084),
          ),
          fontFamily: 'Muli',
        ),
        initialRoute: '/',
        onGenerateRoute: Routes.generateRoute,
      ),
    );
  }
}

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class ProductState with ChangeNotifier {
  bool _pageFiltered = false;
  var _initialQuery = Firestore.instance.collection('productos').snapshots();
  var _currentQuery;
  List<bool> _filterSelected = [];

  ProductState() {
    _initProductState();
  }

  Stream<QuerySnapshot> getInitialQuery() => _initialQuery;

  Stream<QuerySnapshot> getCurrentQuery() => _currentQuery;

  bool isPageFiltered() => _pageFiltered;

  List<bool> getListSelectedFilters() {
    return _filterSelected;
  }

  void setCurrentQuery(category) {
    _currentQuery = Firestore.instance
        .collection('productos')
        .where('categoria', isEqualTo: category)
        .snapshots();
    notifyListeners();
  }

  void changeValueSelected(index) {
    _filterSelected[index] = !_filterSelected[index];
    notifyListeners();
  }

  void addFilterSelectList(List<bool> selected) {
    _filterSelected = selected;
  }

  void pageFilteredOn() {
    _pageFiltered = true;
    notifyListeners();
  }

  void pageFilteredOff() {
    _pageFiltered = false;
    notifyListeners();
  }

  bool someFilterIsSelected() => _filterSelected.contains(true);

  void _initProductState() async {
    _pageFiltered = false;
    notifyListeners();
  }
}

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:unilago_prototype/states/conectivity_services.dart';

import 'LocalDatabase/item_cartDAO.dart';

class CartState with ChangeNotifier {
  String _user;
  SharedPreferences _prefs;
  List<ItemCartDAO> _itemsCart = [];
  List<ItemCartDAO> getItemsCart() => _itemsCart;

  CartState() {
    cartState();
    _user = 'Anonimo';
  }

  Stream<QuerySnapshot> getShoppingNumber(userUid) {
    return Firestore.instance
        .collection('usuarios')
        .document(userUid)
        .collection('compras')
        .snapshots();
  }

  List<dynamic> getPayMap() {
    var result = [];
    _itemsCart.forEach((item) {
      result.add(item.toPayItem());
    });
    notifyListeners();
    return result;
  }

  int getSubTotalItemsCart() {
    return _itemsCart
        .map((e) => int.parse(e.price.toString()) * e.quantity)
        .fold(0, (a, b) => a + b);
  }

  double getIvaCart() {
    return getSubTotalItemsCart() * 0.19;
  }

  int getNumberItemsCart() {
    return _itemsCart.map((e) => e.quantity).fold(0, (a, b) => a + b);
  }

  double getTotalItemsCart() {
    return getSubTotalItemsCart() + getIvaCart();
  }

  _getUserPreferences() => 'Anonimo';

  void addItemCart(ItemCartDAO value, BuildContext context) {
    bool noAdd = false;
    _itemsCart.forEach((item) {
      if (item.product == value.product) {
        if (item.price == value.price) {
          if (item.store == value.store) {
            noAdd = true;
            item.addQuantity();
            notifyListeners();
          }
        }
      }
    });

    if (!noAdd) {
      _itemsCart.add(value);
      notifyListeners();
    }

    notifyListeners();
    var state = Provider.of<ConnectivityStatus>(context, listen: false);

    if (state != ConnectivityStatus.Offline) {
      //Compobar si los items offline son iguales a los online
//      List<ItemCartDAO> offline = _getOfflineListItemsCart(
//          _prefs.getStringList('${_getUserPreferences()},itemsCart'));
//
//      List<ItemCartDAO> online = _getOnlineListItemsCart(list);
//
//      if (!_validateDBsData(offline, online)) {
//        //Si son iguales agrega en ambas bases de datos
//
//        //Online added
//        Firestore.instance
//            .collection('usuarios')
//            .document(currentUser.uid)
//            .updateData({
//          'carrito': _getFirebaseList(_itemsCart),
//        });
//
//        //Offline added
//        _prefs.setStringList(
//            'Anonimo,itemsCart', _getPreferencesList(_itemsCart));
//      } else {
//        //Si no nos iguales agrego a la db online los items de la offline
//        // y luego guardo todos los datos de la db online en la db offline

//      }
      _prefs.setStringList('${_getUserPreferences()},itemsCart',
          _getPreferencesList(_itemsCart));
    } else {
      _prefs.setStringList('${_getUserPreferences()},itemsCart',
          _getPreferencesList(_itemsCart));
    }
    notifyListeners();
  }

  bool _validateDBsData(List<ItemCartDAO> offline, List<ItemCartDAO> online) {
    print('Offline : $offline , Online : $online');
    bool result = false;
    for (int i = 0; i < offline.length && !result; i++) {
      bool current = false;
      for (int j = 0; j < online.length && !result; j++) {
        var item = offline[i];
        var value = online[j];
        if (item.product == value.product) {
          if (item.price == value.price) {
            if (item.store == value.store) {
              current = true;
            } else {
              current = false;
            }
          }
        }
      }
      result = current;
    }
    return result;
  }

  List<ItemCartDAO> _getOnlineListItemsCart(list) {
    if (list == []) {
      List<ItemCartDAO> result = [];
      list.forEach((element) {
        //Each map
        print(element);
        if (element != null || element.toString().isNotEmpty) {
          result.add(ItemCartDAO.fromMap(element));
        }
      });
      return result;
    } else {
      return [];
    }
  }

  void deleteItemCart(ItemCartDAO value, BuildContext context) {
    if (value.getQuantity() == 1) {
      _itemsCart.remove(value);
      notifyListeners();
    } else {
      value.reduceQuantity();
      notifyListeners();
    }

    var state = Provider.of<ConnectivityStatus>(context, listen: false);
    if (state != ConnectivityStatus.Offline) {
      //Add Online DB
      _prefs.setStringList('${_getUserPreferences()},itemsCart',
          _getPreferencesList(_itemsCart));
    } else {
      _prefs.setStringList('${_getUserPreferences()},itemsCart',
          _getPreferencesList(_itemsCart));
    }
    print(_user);
  }

  void clearItemsCart() {
    //Offline
    _prefs.setStringList('${_getUserPreferences()},itemsCart', List());

    //Online

    _itemsCart = [];
    notifyListeners();
  }

  void cartState() async {
    _prefs = await SharedPreferences.getInstance();
    if (_prefs.containsKey('${_getUserPreferences()},itemsCart')) {
      _itemsCart = _getOfflineListItemsCart(
          _prefs.getStringList('${_getUserPreferences()},itemsCart'));
      notifyListeners();
    } else {
      _prefs.setStringList('${_getUserPreferences()},itemsCart', List());
      _itemsCart = [];
      notifyListeners();
    }
  }

  List<Map<String, dynamic>> _getFirebaseList(List<ItemCartDAO> list) {
    List<Map<String, dynamic>> result = [];
    list.forEach((element) {
      result.add(element.toOnlineDB());
    });
    return result;
  }

  List<String> _getPreferencesList(List<ItemCartDAO> list) {
    List<String> result = [];
    list.forEach((element) {
      result.add(element.toString());
    });
    return result;
  }

  List<ItemCartDAO> _getOfflineListItemsCart(List<String> list) {
    if (list.length > 0) {
      List<ItemCartDAO> result = [];
      list.forEach((element) {
        result.add(ItemCartDAO.fromString(element));
      });
      return result;
    } else {
      return [];
    }
  }
}

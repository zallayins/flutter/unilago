class ItemCartDAO {
  String product;
  String store;
  int quantity;
  int price;
  String numeral;
  String image;

  ItemCartDAO(
      {this.image,
      this.product,
      this.store,
      this.quantity,
      this.price,
      this.numeral});

  ItemCartDAO.fromString(String data) {
    List<String> splitted = data.split(',');
    print(splitted[0]);
    this.product = splitted[0];
    this.store = splitted[1];
    this.price = int.parse(splitted[2]);
    this.numeral = splitted[3];
    this.quantity = int.parse(splitted[4]);
    this.image = splitted[5];
  }
  ItemCartDAO.fromMap(Map<String, dynamic> data) {
    this.product = data['producto'];
    this.store = data['tienda'];
    this.price = data['precio'];
    this.numeral = data['numeral'];
    this.quantity = data['cantidad'];
    this.image = data['imagen'];
  }

  int getQuantity() => quantity;

  void addQuantity() {
    quantity++;
  }

  void reduceQuantity() {
    quantity--;
  }

  @override
  String toString() {
    return "${this.product},${this.store},${this.price},${this.numeral},${this.quantity},${this.image}";
  }

  Map<String, dynamic> toOnlineDB() {
    return {
      'cantidad': quantity,
      'producto': this.product,
      'tienda': this.store,
      'numeral': this.numeral,
      'precio': this.price,
      'imagen': this.image,
    };
  }

  Map<String, dynamic> toPayItem() {
    return {
      'cantidad': quantity,
      'producto': this.product,
      'tienda': this.store,
      'numeral': this.numeral,
      'precio': this.price,
      'imagen': this.image,
    };
  }
}

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:unilago_prototype/pages/UserProfile/user.dart';

class LoginState with ChangeNotifier {
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  final FirebaseAuth _auth = FirebaseAuth.instance;

  SharedPreferences _prefs;
  FirebaseUser _user;
  bool _loggedIn = false;
  bool _loading = true;
  bool _error = false;
  bool _createUserPage = false;
  bool _loggedByEmail = false;

  LoginState() {
    loginState();
  }

  bool isLoggedByEmail() => _loggedByEmail;

  FirebaseUser currentUser() => _user;

  bool isLoading() => _loading;

  bool isLogged() => _loggedIn;

  bool hasError() => _error;

  bool isCreatedUserPage() => _createUserPage;

  void notLoggedByEmail() {
    _loggedByEmail = false;
    notifyListeners();
  }

  void loggedByEmail() {
    _loggedByEmail = true;
    notifyListeners();
  }

  void doNotCreateUserPage() {
    _createUserPage = false;
    notifyListeners();
  }

  void createUserPage() {
    _createUserPage = true;
    notifyListeners();
  }

  //Error displayed when submit grown data on the form login with email and password

  void noError() {
    if (_error) {
      _error = false;
      notifyListeners();
    }
  }

  // Login With Google

  void logInWithGoogle() async {
    _loading = true;
    notifyListeners();
    try {
      _user = await _handleSignIn();
      if (Firestore.instance
          .collection('usuarios')
          .document(_user.uid)
          .documentID
          .isNotEmpty) {
        Firestore.instance
            .collection('usuarios')
            .document(_user.uid)
            .updateData({
          "id": _user.uid,
          "nombre": _user.displayName,
          "correo": _user.email,
        });
      } else {
        Firestore.instance.collection('usuarios').document(_user.uid).setData({
          "id": _user.uid,
          "nombre": _user.displayName,
          "correo": _user.email,
          "direccion": "",
          "celular": 0000000000,
          "carrito": List(),
        });
      }

      _loading = false;
      if (_user != null) {
        _prefs.setBool('isLoggedWithGoogle', true);
        _loggedIn = true;
        notifyListeners();
      } else {
        _loggedIn = false;
        notifyListeners();
      }
    } catch (e) {
      _user = null;
      _loading = false;
      _loggedIn = false;
      notifyListeners();
    }
  }

  void logOutWithGoogle() async {
    await _prefs.remove('isLoggedWithGoogle');
    await _googleSignIn.signOut().then((_) async => _user = null);
    _loggedIn = false;

    notifyListeners();
  }

  // Login With Email and Password

  void loginWithEmailAndPassword(
      String email, String password, bool remember) async {
    print('email: $email\npassword: $password \nremember: $remember');
    try {
      _prefs.clear();
      _error = false;
      var result = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      _user = result.user;
    } catch (e) {
      _error = true;
      notifyListeners();
    }

    _loading = true;
    notifyListeners();
    Future.delayed(Duration(seconds: 1)).whenComplete(() {
      _loading = false;
      notifyListeners();
    });

    if (_user != null) {
      if (remember)
        _prefs.setString('isLoggedWithEmailAndPassword', _user.email);
      _loggedIn = true;
      notifyListeners();
    } else {
      _loggedIn = false;
      notifyListeners();
    }
  }

  void logOutWithEmailAndPassword() async {
    _loading = true;
    notifyListeners();

    await _prefs.remove('isLoggedWithEmailAndPassword');
    await _auth.signOut();
    _user = await _auth.currentUser();

    _loading = false;
    _loggedIn = false;
    notifyListeners();
  }

  //Create User with Email and Password
  void registerWithEmailAndPassword(String email, String password) async {
    try {
      _error = false;
      var result = await _auth.createUserWithEmailAndPassword(
          email: email, password: password);
      _user = result.user;

//      UserUpdateInfo userUpdateInfo = new UserUpdateInfo();
//      userUpdateInfo.displayName = 'Usuario Anonimo';
//      _user.updateProfile(userUpdateInfo);

      Firestore.instance.collection('usuarios').document(_user.uid).setData({
        "id": _user.uid,
        "nombre": "Usuario Anonimo",
        "correo": _user.email,
        "direccion": "",
        "celular": 0000000000,
        "carrito": List(),
      });
    } catch (e) {
      _error = true;
      notifyListeners();
    }

    _loading = true;
    notifyListeners();
    _prefs.setString('isLoggedWithEmailAndPassword', _user.email);
    Future.delayed(Duration(seconds: 1)).whenComplete(() {
      _loading = false;
      notifyListeners();
    });

    _loggedIn = true;
    notifyListeners();
  }

  Future<FirebaseUser> _handleSignIn() async {
    final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
    final GoogleSignInAuthentication googleAuth =
        await googleUser.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    final FirebaseUser user =
        (await _auth.signInWithCredential(credential)).user;
    print("signed in " + user.displayName);
    return user;
  }

  void editProfileData(UserData data) {
    Firestore.instance.collection('usuarios').document(_user.uid).updateData({
      "id": _user.uid,
      "nombre": data.name,
      "correo": data.mail,
      "direccion": data.address,
      "celular": data.cellphoneNumber,
    });
    notifyListeners();
  }

  void loginState() async {
    _prefs = await SharedPreferences.getInstance();
    if (_prefs.containsKey('isLoggedWithGoogle') ||
        _prefs.containsKey('isLoggedWithEmailAndPassword')) {
      _user = await _auth.currentUser();
      _loggedIn = _user != null;
      _loading = false;
      notifyListeners();
    } else {
      _loading = false;
      notifyListeners();
    }
  }
}

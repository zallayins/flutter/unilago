import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';

ProgressDialog invokeDialogProgress(
    BuildContext context, String message, Widget progress) {
  ProgressDialog pr;
  pr = new ProgressDialog(context);
  pr.style(
    message: message,
    borderRadius: 10.0,
    backgroundColor: Colors.white,
    progressWidget: progress,
    elevation: 10.0,
    insetAnimCurve: Curves.easeInOut,
    progress: 0.0,
    maxProgress: 25.0,
    progressTextStyle: TextStyle(
        color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
    messageTextStyle: TextStyle(
        color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600),
    textAlign: TextAlign.center,
  );
  return pr;
}

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:unilago_prototype/pages/DetailProduct/detail.dart';
import 'package:unilago_prototype/pages/LogIn/login.dart';
import 'package:unilago_prototype/pages/UserProfile/Purchases/purchases.dart';
import 'package:unilago_prototype/pages/UserProfile/edit.dart';
import 'package:unilago_prototype/pages/UserProfile/user.dart';
import 'package:unilago_prototype/pages/bottom_navbar.dart';
import 'package:unilago_prototype/pages/homepage/Filters/list_itemsFiltered.dart';
import 'package:unilago_prototype/pages/recomendation.dart';
import 'package:unilago_prototype/pages/shoppingCart/shopping_cart.dart';
import 'package:unilago_prototype/states/LocalDatabase/item_cartDAO.dart';

class Routes {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => BottomNavBar());

      case '/purchases':
        return MaterialPageRoute(builder: (_) => PurchasesList());

      case '/loginPay':
        return MaterialPageRoute(
            builder: (_) => LoginPage(
                  cart: true,
                ));

      case '/recomendation':
        if (args is Widget) {
          return MaterialPageRoute(
            builder: (_) => Recomendation(),
          );
        }
        return MaterialPageRoute(builder: (_) => Recomendation());

      case '/detail':
        if (args is DocumentSnapshot) {
          return MaterialPageRoute(builder: (_) => DetailProduct(data: args));
        }
        return _errorRoute();

      case '/listItemsFiltered':
        if (args is String) {
          return MaterialPageRoute(
              builder: (_) => ListItemsFiltered(category: args));
        }
        return _errorRoute();

      case '/editProfile':
        if (args is UserData) {
          return MaterialPageRoute(builder: (_) => EditProfile(user: args));
        }
        return _errorRoute();

      case '/shopping_cart':
        if (args is ItemCartDAO) {
          return MaterialPageRoute(builder: (_) => ShoppingCart(data: args));
        }
        return MaterialPageRoute(builder: (_) => ShoppingCart());

      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
          appBar: AppBar(
            title: Text("Error"),
          ),
          body: Center(
              child: Text('ERROR Path',
                  style:
                      TextStyle(fontSize: 15.0, fontWeight: FontWeight.w500))));
    });
  }
}

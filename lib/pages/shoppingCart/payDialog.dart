import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';
import 'package:unilago_prototype/shared/progressDialog.dart';
import 'package:unilago_prototype/states/cart_state.dart';

class PayDialog extends StatelessWidget {
  final FirebaseUser user;

  const PayDialog({Key key, this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ProgressDialog pr = invokeDialogProgress(
      context,
      '...',
      SpinKitChasingDots(
        color: Theme.of(context).primaryColor,
        duration: Duration(seconds: 2),
      ),
    );

    return AlertDialog(
      scrollable: true,
      title: Text('¿Desea continuar con el pago?'),
      actions: <Widget>[
        MaterialButton(
          elevation: 5.0,
          child: Text('Cancelar'),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        MaterialButton(
          elevation: 5.0,
          child: Text('Aceptar'),
          onPressed: () {
            //Validate connexion to internet

            Firestore.instance
                .collection('usuarios')
                .document(user.uid)
                .collection('compras')
                .document()
                .setData({
              'fecha': Timestamp.fromDate(DateTime.now()),
              'items':
                  Provider.of<CartState>(context, listen: false).getPayMap(),
              'total': Provider.of<CartState>(context, listen: false)
                  .getTotalItemsCart(),
            });
            Navigator.pop(context);
            Provider.of<CartState>(context, listen: false).clearItemsCart();
            pr.style(message: 'Validando con el banco');
            pr.show();
            Future.delayed(Duration(seconds: 1)).then((value) {
              pr.update(message: '¡Compra Existosa!');
              Future.delayed(Duration(seconds: 1)).then((value) => pr.hide());
            });

            // Add to pay document
          },
        ),
      ],
    );
  }
}

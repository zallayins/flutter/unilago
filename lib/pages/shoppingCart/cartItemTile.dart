import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:unilago_prototype/states/LocalDatabase/item_cartDAO.dart';
import 'package:unilago_prototype/states/cart_state.dart';

class CartItemTile extends StatelessWidget {
  final ItemCartDAO itemCart;

  const CartItemTile({Key key, this.itemCart}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final heightPhone = MediaQuery.of(context).size.height;

    return GridTile(
      header: Container(
        height: heightPhone < 700 ? heightPhone * 0.244 : heightPhone * 0.226,
        decoration: BoxDecoration(
          color: Colors.black45,
          borderRadius: BorderRadius.all(
            Radius.circular(20),
          ),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(
                left: 15.0,
                top: 10.0,
              ),
              child: Text(
                itemCart.numeral.toString(),
                style: TextStyle(
                  color: Colors.blue[100],
                  fontSize: 22,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            IconButton(
              icon: Icon(
                Icons.close,
                color: Colors.black54,
              ),
              onPressed: () {
                Provider.of<CartState>(context, listen: false)
                    .deleteItemCart(itemCart, context);
              },
            ),
          ],
        ),
      ),
      footer: Column(
        children: <Widget>[
          ListTile(
            title: Text(
              itemCart.product.toString(),
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w700,
              ),
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
            subtitle: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  '\$ ${itemCart.price.toString()}',
                  style: TextStyle(
                    color: Colors.grey[200],
                  ),
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'X',
                      style: TextStyle(
                        color: Colors.red[200],
                        fontWeight: FontWeight.w700,
                        fontSize: 10,
                      ),
                    ),
                    Text(
                      itemCart.quantity.toString(),
                      style: TextStyle(
                        color: Colors.red[200],
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
      child: Container(
          height: heightPhone * 0.244,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(
              Radius.circular(20),
            ),
          ),
          child: CachedNetworkImage(
            imageUrl: itemCart.image.toString(),
            fit: BoxFit.contain,
            placeholder: (context, url) =>
                Center(child: CircularProgressIndicator()),
            errorWidget: (context, url, error) => Icon(Icons.error),
          )),
    );
  }
}

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:unilago_prototype/states/LocalDatabase/item_cartDAO.dart';

class ItemCart {
  final Map<String, dynamic> price;
  final DocumentSnapshot productInfo;
  final DocumentSnapshot storeInfo;
  int quantity;

  ItemCart({this.price, this.storeInfo, this.productInfo}) {
    quantity = 1;
  }

  int getQuantity() => quantity;

  void addQuantity() {
    quantity++;
  }

  void reduceQuantity() {
    quantity--;
  }

  Map<String, dynamic> toPayItem() {
    return {
      'cantidad': quantity,
      'producto': productInfo['nombre'].toString(),
      'tienda': storeInfo['nombre'],
      'numeral': storeInfo['numeral'],
      'precio': int.parse(price['precio'].toString()),
      'imagen': productInfo['imagen'],
    };
  }

  Map<String, dynamic> toOnlineDB() {
    return {
      'cantidad': quantity,
      'producto': productInfo['nombre'].toString(),
      'tienda': storeInfo['nombre'],
      'numeral': storeInfo['numeral'],
      'precio': int.parse(price['precio'].toString()),
      'imagen': productInfo['imagen'],
    };
  }

  ItemCartDAO toOfflineDB() {
    return ItemCartDAO(
      product: productInfo['nombre'].toString(),
      quantity: quantity,
      store: storeInfo['nombre'],
      numeral: storeInfo['numeral'],
      price: int.parse(price['precio'].toString()),
      image: productInfo['imagen'],
    );
  }
}

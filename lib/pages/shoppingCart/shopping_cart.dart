import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:provider/provider.dart';
import 'package:unilago_prototype/pages/shoppingCart/cartItemTile.dart';
import 'package:unilago_prototype/pages/shoppingCart/payDialog.dart';
import 'package:unilago_prototype/states/LocalDatabase/item_cartDAO.dart';
import 'package:unilago_prototype/states/cart_state.dart';
import 'package:unilago_prototype/states/conectivity_services.dart';
import 'package:unilago_prototype/states/login_state.dart';

class ShoppingCart extends StatefulWidget {
  final ItemCartDAO data;
  ShoppingCart({Key key, this.data}) : super(key: key);

  @override
  _ShoppingCartState createState() => _ShoppingCartState();
}

class _ShoppingCartState extends State<ShoppingCart> {
  @override
  Widget build(BuildContext context) {
    final widthPhone = MediaQuery.of(context).size.width;
    final heightPhone = MediaQuery.of(context).size.height;
    var connectionStatus =
        Provider.of<ConnectivityStatus>(context, listen: false);

    final styleShoppingCart = TextStyle(
      color: Colors.black,
      fontWeight: FontWeight.w200,
      fontSize: 18,
      letterSpacing: 1.5,
    );

    _getPayAlertDialog(context) {
      return showDialog(
          context: context,
          builder: (context) {
            return PayDialog(
              user:
                  Provider.of<LoginState>(context, listen: false).currentUser(),
            );
          });
    }

    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          FlatButton(
            onPressed: () {
              Provider.of<CartState>(context, listen: false).clearItemsCart();
            },
            child: Text('Limpiar'),
          )
        ],
        centerTitle: true,
        title: Text(
          'Carrito',
          style: TextStyle(color: Colors.black),
        ),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.grey[600],
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        elevation: 0.0,
        backgroundColor: Theme.of(context).canvasColor,
      ),
      body: OfflineBuilder(
        connectivityBuilder: (
          BuildContext context,
          ConnectivityResult connectivity,
          Widget child,
        ) {
          final bool connected = connectivity != ConnectivityResult.none;
          return new Stack(
            fit: StackFit.expand,
            children: [
              Center(child: child),
              Positioned(
                height: 24.0,
                left: 0.0,
                right: 0.0,
                child: connected
                    ? Container()
                    : Container(
                        color:
                            connected ? Color(0xFF00EE44) : Color(0xFFEE4400),
                        child: Center(
                          child: Text(
                              "${connected ? 'ONLINE' : 'Sin conexión a internet'}"),
                        ),
                      ),
              ),
            ],
          );
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: heightPhone * 0.55,
              width: widthPhone * 0.9,
              child: _buildListCardProducts(heightPhone, widthPhone),
            ),
            Align(
              alignment: Alignment.center,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text(
                    'Subtotal',
                    style: styleShoppingCart,
                  ),
                  Text(
                    (Provider.of<CartState>(context)
                        .getSubTotalItemsCart()
                        .toString()),
                    style: styleShoppingCart,
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text(
                    'IVA     ',
                    style: styleShoppingCart,
                  ),
                  Text(
                    (Provider.of<CartState>(context)
                        .getIvaCart()
                        .toStringAsFixed(2)),
                    style: styleShoppingCart,
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text(
                    'Total   ',
                    style: styleShoppingCart.copyWith(
                      fontSize: 22,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    Provider.of<CartState>(context)
                        .getTotalItemsCart()
                        .toStringAsFixed(2),
                    style: styleShoppingCart.copyWith(
                      fontSize: 22,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: heightPhone * 0.05,
            ),
            FlatButton(
              padding: EdgeInsets.symmetric(horizontal: 60.0, vertical: 12.0),
              shape: StadiumBorder(),
              // ignore: missing_return
              onPressed: () {
                if (connectionStatus != ConnectivityStatus.Offline) {
                  if (Provider.of<CartState>(context, listen: false)
                      .getItemsCart()
                      .isNotEmpty) {
                    var currentUser =
                        Provider.of<LoginState>(context, listen: false)
                            .currentUser();
                    if (currentUser == null) {
                      Navigator.of(context).pushNamed('/loginPay');
                    } else {
                      _getPayAlertDialog(context);
                    }
                  } else {
                    return Flushbar(
                      isDismissible: false,
                      blockBackgroundInteraction: true,
                      margin: EdgeInsets.fromLTRB(widthPhone * 0.2, 0,
                          widthPhone * 0.2, heightPhone * 0.05),
                      borderRadius: 100,
                      flushbarStyle: FlushbarStyle.FLOATING,
                      padding: EdgeInsets.all(10),
                      messageText: Text(
                        'No hay items para pagar',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      duration: Duration(milliseconds: 1300),
                    )..show(context);
                  }
                } else {
                  Flushbar(
                    isDismissible: false,
                    blockBackgroundInteraction: true,
                    borderRadius: 100,
                    flushbarStyle: FlushbarStyle.GROUNDED,
                    flushbarPosition: FlushbarPosition.TOP,
                    titleText: Text(
                      'Sin conexión',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.w800,
                      ),
                    ),
                    messageText: Text(
                      'Necesitas internet para realizar el pago',
                      textAlign: TextAlign.justify,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    duration: Duration(milliseconds: 1500),
                  )..show(context);
                }
              },
              child: Text(
                'Pagar',
                style: styleShoppingCart.copyWith(color: Colors.white),
              ),
              color: Colors.blue,
            ),
          ],
        ),
      ),
    );
  }

  _buildListCardProducts(heightPhone, widthPhone) {
    var items = Provider.of<CartState>(context).getItemsCart();
    return items.isEmpty
        ? Container(
            child: Center(
              child: Text('No hay items en el carrito'),
            ),
          )
        : GridView.builder(
            itemCount: items.length,
            itemBuilder: (context, index) {
              ItemCartDAO itemCart = items[index];
              return CartItemTile(
                itemCart: itemCart,
              );
            },
            scrollDirection: Axis.vertical,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              mainAxisSpacing: 10.0,
              crossAxisSpacing: 10.0,
            ),
          );
  }
}

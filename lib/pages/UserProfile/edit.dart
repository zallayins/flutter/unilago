import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:provider/provider.dart';
import 'package:unilago_prototype/pages/UserProfile/user.dart';
import 'package:unilago_prototype/states/conectivity_services.dart';
import 'package:unilago_prototype/states/login_state.dart';

class EditProfile extends StatefulWidget {
  final UserData user;

  const EditProfile({Key key, this.user}) : super(key: key);

  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  TextEditingController nameController;
  TextEditingController mailController;
  TextEditingController addressController;
  TextEditingController cellphoneNumberController;
  var _name;
  var _mail;
  var _address;
  var _cell;

  @override
  void dispose() {
    nameController.dispose();
    mailController.dispose();
    addressController.dispose();
    cellphoneNumberController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var status = Provider.of<ConnectivityStatus>(context);

    final heightPhone = MediaQuery.of(context).size.height;
    final widthPhone = MediaQuery.of(context).size.width;
    nameController = new TextEditingController(text: widget.user.name);
    mailController = new TextEditingController(text: widget.user.mail);
    addressController = new TextEditingController(text: widget.user.address);
    cellphoneNumberController = widget.user.cellphoneNumber != 0
        ? new TextEditingController(
            text: widget.user.cellphoneNumber.toString())
        : new TextEditingController();

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Colors.grey[600],
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          elevation: 0,
          backgroundColor: Theme.of(context).canvasColor,
          title: Text(
            'Editar perfil',
            style: TextStyle(
              color: Colors.grey[600],
              fontSize: 20,
              fontWeight: FontWeight.w400,
            ),
          ),
          centerTitle: true,
        ),
        body: OfflineBuilder(
          connectivityBuilder: (
            BuildContext context,
            ConnectivityResult connectivity,
            Widget child,
          ) {
            final bool connected = connectivity != ConnectivityResult.none;
            return new Stack(
              fit: StackFit.expand,
              children: [
                Center(child: child),
                Positioned(
                  height: 24.0,
                  left: 0.0,
                  right: 0.0,
                  child: connected
                      ? Container()
                      : Container(
                          color:
                              connected ? Color(0xFF00EE44) : Color(0xFFEE4400),
                          child: Center(
                            child: Text(
                                "${connected ? 'ONLINE' : 'Sin conexión a internet'}"),
                          ),
                        ),
                ),
              ],
            );
          },
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                _formProfile(heightPhone, widthPhone),

//                _formProfile(heightPhone, widthPhone),
                SizedBox(
                  height: heightPhone * 0.05,
                ),
//                status == ConnectivityStatus.Offline
//                    ? FlatButton(
//                        padding:
//                            EdgeInsets.symmetric(horizontal: widthPhone * 0.1),
//                        shape: StadiumBorder(),
//                        child: Text(
//                          'Guardar datos',
//                          style: TextStyle(
//                            fontWeight: FontWeight.bold,
//                            color: Colors.white,
//                            fontSize: 18,
//                          ),
//                        ),
//                        onPressed: () {
//                          Flushbar(
//                            isDismissible: false,
//                            blockBackgroundInteraction: true,
//                            borderRadius: 100,
//                            flushbarStyle: FlushbarStyle.GROUNDED,
//                            flushbarPosition: FlushbarPosition.TOP,
//                            titleText: Text(
//                              'Sin conexión a internet',
//                              style: TextStyle(
//                                color: Colors.white,
//                                fontSize: 18,
//                                fontWeight: FontWeight.w800,
//                              ),
//                            ),
//                            messageText: Text(
//                              'Necesitas tener conexión a internet para cerrar sesión',
//                              textAlign: TextAlign.justify,
//                              style: TextStyle(
//                                color: Colors.white,
//                                fontSize: 16,
//                                fontWeight: FontWeight.w500,
//                              ),
//                            ),
//                            duration: Duration(milliseconds: 1500),
//                          )..show(context);
//                        },
//                        color: Theme.of(context).primaryColor,
//                      ):
                FlatButton(
                  padding: EdgeInsets.symmetric(horizontal: widthPhone * 0.1),
                  shape: StadiumBorder(),
                  child: Text(
                    'Guardar datos',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontSize: 18,
                    ),
                  ),
                  onPressed: () {
                    if (formKey.currentState.validate()) {
                      UserData data = UserData(
                        name: nameController.text,
                        mail: mailController.text,
                        address: addressController.text,
                        cellphoneNumber:
                            int.parse(cellphoneNumberController.text),
                      );

                      Provider.of<LoginState>(context, listen: false)
                          .editProfileData(data);
                      formKey.currentState.reset();
                      Navigator.pop(context);
                    }
                  },
                  color: Theme.of(context).primaryColor,
                ),
                SizedBox(
                  height: heightPhone * 0.05,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _formProfile(heightPhone, widthPhone) {
    print('$heightPhone , $widthPhone');
    return Form(
      key: formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: heightPhone * 0.03,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(bottom: heightPhone * 0.02),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    height:
                        widthPhone > 400 ? widthPhone * 0.35 : widthPhone * 0.3,
                    width:
                        widthPhone > 400 ? widthPhone * 0.35 : widthPhone * 0.3,
                    decoration: BoxDecoration(
                      color: Colors.grey[400],
                      border: Border.all(
                        color: Colors.white,
                        width: 3,
                      ),
                      shape: BoxShape.circle,
                    ),
                    child: Container(
                      height: widthPhone * 0.3,
                      width: widthPhone * 0.3,
                      decoration: BoxDecoration(
                        color: Colors.transparent,
                        image: DecorationImage(
                          fit: BoxFit.fill,
                          image: Provider.of<LoginState>(context)
                                      .currentUser()
                                      .photoUrl ==
                                  null
                              ? AssetImage('assets/images/user.png')
                              : NetworkImage(
                                  Provider.of<LoginState>(context)
                                      .currentUser()
                                      .photoUrl,
                                ),
                        ),
                        shape: BoxShape.circle,
                      ),
                    ),
                  ),
                ),
              ),
//              FlatButton(
//                shape: StadiumBorder(),
//                color: Colors.grey[400],
//                child: Text('Cambiar foto'),
//                onPressed: () {
//                  Flushbar(
//                    isDismissible: false,
//                    blockBackgroundInteraction: true,
//                    borderRadius: 100,
//                    flushbarStyle: FlushbarStyle.GROUNDED,
//                    flushbarPosition: FlushbarPosition.TOP,
//                    titleText: Text(
//                      'No entra en este sprint',
//                      style: TextStyle(
//                        color: Colors.white,
//                        fontSize: 18,
//                        fontWeight: FontWeight.w800,
//                      ),
//                    ),
//                    messageText: Text(
//                      'Se necesita un semestre más para resolver esta carácteristica',
//                      textAlign: TextAlign.justify,
//                      style: TextStyle(
//                        color: Colors.white,
//                        fontSize: 16,
//                        fontWeight: FontWeight.w500,
//                      ),
//                    ),
//                    duration: Duration(milliseconds: 1500),
//                  )..show(context);
//                },
//              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: SizedBox(
              height:
                  heightPhone > 700 ? heightPhone * 0.093 : heightPhone * 0.11,
              width: widthPhone * 0.8,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    ' Nombre',
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  TextFormField(
                    onEditingComplete: () {
                      nameController.text = _name;
                    },
                    onChanged: (value) {
                      _name = value;
                    },
                    onSaved: (value) {
                      _name = value;
                    },
                    onFieldSubmitted: (value) {
                      _name = value;
                    },
                    style: nameController.text.isEmpty
                        ? TextStyle()
                        : TextStyle(
                            fontWeight: FontWeight.w600,
                            color: Theme.of(context).primaryColor,
                          ),
                    maxLines: 1,
                    controller: nameController,
                    // ignore: missing_return
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Ingresa un nombre de usuario';
                      }
                    },
                    decoration: InputDecoration(
                        hintText: 'Ej: Usuario Anonimo',
                        contentPadding: EdgeInsets.symmetric(horizontal: 15),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(40),
                          borderSide:
                              BorderSide(color: Colors.black, width: 20),
                        )),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            height: heightPhone * 0.03,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: SizedBox(
              height:
                  heightPhone > 700 ? heightPhone * 0.093 : heightPhone * 0.11,
              width: widthPhone * 0.8,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    ' Correo',
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  TextFormField(
                    style: mailController.text.isEmpty
                        ? TextStyle()
                        : TextStyle(
                            fontWeight: FontWeight.w600,
                            color: Theme.of(context).primaryColor,
                          ),
                    maxLines: 1,
                    controller: mailController,
                    onEditingComplete: () {
                      mailController.text = _mail;
                    },
                    onChanged: (value) {
                      _mail = value;
                    },
                    onSaved: (value) {
                      _mail = value;
                    },
                    onFieldSubmitted: (value) {
                      _mail = value;
                    },
                    // ignore: missing_return
                    validator: (value) {
                      if (value.isEmpty || !value.contains('@')) {
                        return 'Ingresa un correo valido';
                      }
                    },
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                        hintText: 'Ej: ejemplo@uno.com',
                        contentPadding: EdgeInsets.symmetric(horizontal: 15),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(40),
                          borderSide:
                              BorderSide(color: Colors.black, width: 20),
                        )),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            height: heightPhone * 0.03,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: SizedBox(
              height:
                  heightPhone > 700 ? heightPhone * 0.093 : heightPhone * 0.11,
              width: widthPhone * 0.8,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    ' Dirección',
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  TextFormField(
                    style: addressController.text.isEmpty
                        ? TextStyle()
                        : TextStyle(
                            fontWeight: FontWeight.w600,
                            color: Theme.of(context).primaryColor,
                          ),
                    maxLines: 1,
                    onEditingComplete: () {
                      addressController.text = _address;
                    },
                    onChanged: (value) {
                      _address = value;
                    },
                    onSaved: (value) {
                      _address = value;
                    },
                    onFieldSubmitted: (value) {
                      _address = value;
                    },
                    controller: addressController,
                    decoration: InputDecoration(
                        hintText: 'Ej: Calle 39 C sur # 82 - 21',
                        contentPadding: EdgeInsets.symmetric(horizontal: 15),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(40),
                          borderSide:
                              BorderSide(color: Colors.black, width: 20),
                        )),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            height: heightPhone * 0.03,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: SizedBox(
              height:
                  heightPhone > 700 ? heightPhone * 0.093 : heightPhone * 0.11,
              width: widthPhone * 0.8,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    ' Número de celular',
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  TextFormField(
                    style: cellphoneNumberController.text.isEmpty
                        ? TextStyle()
                        : TextStyle(
                            fontWeight: FontWeight.w600,
                            color: Theme.of(context).primaryColor,
                          ),
                    maxLines: 1,
                    keyboardType: TextInputType.number,
                    onEditingComplete: () {
                      cellphoneNumberController.text = _cell;
                    },
                    onChanged: (value) {
                      _cell = value;
                    },
                    onSaved: (value) {
                      _cell = value;
                    },
                    onFieldSubmitted: (value) {
                      _cell = value;
                    },
                    // ignore: missing_return
                    validator: (value) {
                      if (value.contains('.') ||
                          value.contains(' ') ||
                          value.contains(',') ||
                          value.contains('-')) {
                        return 'Ingrese unicamente numeros';
                      } else if (value.length > 10) {
                        return 'El número es muy grande';
                      }
                    },
                    controller: cellphoneNumberController,
                    decoration: InputDecoration(
                        hintText: 'Ej: 3102285549',
                        contentPadding: EdgeInsets.symmetric(horizontal: 15),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(40),
                          borderSide:
                              BorderSide(color: Colors.black, width: 20),
                        )),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

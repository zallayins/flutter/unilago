import 'package:flutter/material.dart';
import 'package:unilago_prototype/pages/UserProfile/listShopping.dart';

class Bookings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final heightPhone = MediaQuery.of(context).size.height;
    final widthPhone = MediaQuery.of(context).size.width;
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: widthPhone * 0.05),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'Ultimas compras',
                style: TextStyle(
                  fontSize: 18.0,
                  fontWeight: FontWeight.w500,
                ),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.of(context).pushNamed('/purchases');
                },
                child: Text(
                  'Ver más',
                  style: TextStyle(
                    color: Colors.blue[600],
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: heightPhone * 0.01,
          ),
          Container(
            height: heightPhone * 0.2,
            child: ListShoppingProfile(),
          ),
        ],
      ),
    );
  }
}

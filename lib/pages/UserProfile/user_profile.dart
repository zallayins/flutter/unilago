import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:unilago_prototype/pages/UserProfile/bookings.dart';
import 'package:unilago_prototype/pages/UserProfile/user.dart';
import 'package:unilago_prototype/states/cart_state.dart';
import 'package:unilago_prototype/states/conectivity_services.dart';
import 'package:unilago_prototype/states/login_state.dart';

class UserProfile extends StatefulWidget {
  final bool firstTime;

  const UserProfile({Key key, this.firstTime}) : super(key: key);

  @override
  _UserProfileState createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  @override
  Widget build(BuildContext context) {
    final heightPhone = MediaQuery.of(context).size.height;
    final widthPhone = MediaQuery.of(context).size.width;
    UserData currentData;

    return OfflineBuilder(
      connectivityBuilder: (
        BuildContext context,
        ConnectivityResult connectivity,
        Widget child,
      ) {
        final bool connected = connectivity != ConnectivityResult.none;
        return new Stack(
          fit: StackFit.expand,
          children: [
            child,
            Positioned(
              height: 24.0,
              bottom: kBottomNavigationBarHeight - kToolbarHeight,
              left: 0.0,
              right: 0.0,
              child: connected
                  ? Container()
                  : Container(
                      color: connected
                          ? Color(0xFF00EE44)
                          : Theme.of(context).primaryColor,
                      child: Center(
                        child: Text(
                          "${connected ? 'ONLINE' : 'Sin conexión a internet'}",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
            ),
          ],
        );
      },
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SafeArea(
              child: Container(
                color: Theme.of(context).canvasColor,
                height:
                    heightPhone > 700 ? heightPhone * 0.4 : heightPhone * 0.45,
                width: widthPhone,
                child: Stack(
                  alignment: Alignment.topCenter,
                  children: <Widget>[
                    Container(
                      height: heightPhone > 700
                          ? heightPhone * 0.3
                          : heightPhone * 0.33,
                      color: Colors.blueAccent,
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                        height: heightPhone > 700
                            ? heightPhone * 0.22
                            : heightPhone * 0.25,
                        width: widthPhone * 0.8,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          color: Colors.white,
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                              color: Colors.black54,
                              offset: Offset(0, 3),
                              blurRadius: 7,
                            ),
                          ],
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              child: Container(),
                            ),
                            StreamBuilder<DocumentSnapshot>(
                                stream: Firestore.instance
                                    .collection('usuarios')
                                    .document(Provider.of<LoginState>(context,
                                            listen: false)
                                        .currentUser()
                                        .uid)
                                    .snapshots(),
                                builder: (context, snapshot) {
                                  if (snapshot.connectionState ==
                                          ConnectionState.waiting ||
                                      !snapshot.hasData ||
                                      snapshot.connectionState ==
                                          ConnectionState.none) {
                                    return Container();
                                  }
                                  var userData = snapshot.data;
                                  return Row(
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Spacer(
                                        flex: 2,
                                      ),
                                      Column(
                                        children: <Widget>[
                                          Text(
                                            userData['nombre']
                                                    .toString()
                                                    .isEmpty
                                                ? 'Usuario Anonimo'
                                                : userData['nombre'].toString(),
                                            style: TextStyle(
                                              fontSize:
                                                  heightPhone < 700 ? 19 : 16,
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                          Text(
                                            userData['correo'].toString(),
                                            style: TextStyle(
                                              color: Colors.blueAccent[200],
                                              fontSize:
                                                  heightPhone > 700 ? 16 : 13.2,
                                            ),
                                          ),
                                        ],
                                      ),
                                      Spacer(
                                        flex: 2,
                                      ),
                                    ],
                                  );
                                }),
                            SizedBox(
                              height: heightPhone * 0.02, //heightPhone * 0.012,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    Text(
                                      'Compras',
                                      style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                    SizedBox(
                                      height: heightPhone * 0.01,
                                    ),
                                    StreamBuilder<QuerySnapshot>(
                                        stream: Provider.of<CartState>(context)
                                            .getShoppingNumber(
                                                Provider.of<LoginState>(context,
                                                        listen: false)
                                                    .currentUser()
                                                    .uid),
                                        builder: (context, snapshot) {
                                          if (snapshot.connectionState ==
                                                  ConnectionState.waiting ||
                                              !snapshot.hasData ||
                                              snapshot.connectionState ==
                                                  ConnectionState.none) {
                                            return Text(
                                              '0',
                                              style: TextStyle(
                                                fontSize: 18,
                                                fontWeight: FontWeight.w500,
                                              ),
                                            );
                                          }
                                          var purchases =
                                              snapshot.data.documents.length;
                                          return Text(
                                            purchases.toString(),
                                            style: TextStyle(
                                              fontSize: 18,
                                              fontWeight: FontWeight.w500,
                                            ),
                                          );
                                        }),
                                    SizedBox(
                                      height: heightPhone * 0.01,
                                    ),
                                  ],
                                ),
                                Container(
                                  width: 1.0,
                                  height: heightPhone * 0.04,
                                  color: Colors.black26,
                                ),
                                Column(
                                  children: <Widget>[
                                    Text(
                                      'Reservas',
                                      style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                    SizedBox(
                                      height: heightPhone * 0.01,
                                    ),
                                    Text(
                                      '0',
                                      style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                    SizedBox(
                                      height: heightPhone * 0.01,
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: heightPhone * 0.085),
                      child: Align(
                        alignment: Alignment.center,
                        child: Container(
                          height: widthPhone > 400
                              ? widthPhone * 0.35
                              : widthPhone * 0.3,
                          width: widthPhone > 400
                              ? widthPhone * 0.35
                              : widthPhone * 0.3,
                          decoration: BoxDecoration(
                            color: Colors.grey[400],
                            border: Border.all(
                              color: Colors.white,
                              width: 3,
                            ),
                            shape: BoxShape.circle,
                          ),
                          child: Container(
                            height: widthPhone * 0.3,
                            width: widthPhone * 0.3,
                            decoration: BoxDecoration(
                              color: Colors.transparent,
                              image: DecorationImage(
                                fit: BoxFit.fill,
                                image: Provider.of<LoginState>(context)
                                            .currentUser()
                                            .photoUrl ==
                                        null
                                    ? AssetImage('assets/images/user.png')
                                    : NetworkImage(
                                          Provider.of<LoginState>(context)
                                              .currentUser()
                                              .photoUrl,
                                        ) ??
                                        AssetImage('assets/images/user.png'),
                              ),
                              shape: BoxShape.circle,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      left: widthPhone * 0.05,
                      top: widthPhone * 0.05,
                      child: InkWell(
                        child: StreamBuilder<DocumentSnapshot>(
                            stream: Firestore.instance
                                .collection('usuarios')
                                .document(Provider.of<LoginState>(context,
                                        listen: false)
                                    .currentUser()
                                    .uid)
                                .snapshots(),
                            builder: (context, snapshot) {
                              if (snapshot.connectionState ==
                                      ConnectionState.waiting ||
                                  !snapshot.hasData ||
                                  snapshot.connectionState ==
                                      ConnectionState.none) {
                                return Container();
                              }
                              var userData = snapshot.data;
                              currentData = UserData(
                                name: userData['nombre'].toString() ?? '',
                                mail: userData['correo'].toString() ?? '',
                                address: userData['direccion'].toString() ?? '',
                                cellphoneNumber:
                                    int.parse(userData['celular'].toString()) ??
                                        1234567890,
                              );
                              return Text(
                                'Editar',
                                style: TextStyle(
                                  fontSize: 20.0,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.blue[900],
                                ),
                              );
                            }),
                        onTap: () {
                          setState(() {});
                          Navigator.of(context).pushNamed('/editProfile',
                              arguments: currentData);
                        },
                      ),
                    ),
                    Provider.of<ConnectivityStatus>(context, listen: false) ==
                            ConnectivityStatus.Offline
                        ? Positioned(
                            right: 0,
                            top: 0,
                            child: IconButton(
                              icon: Icon(
                                FontAwesomeIcons.signOutAlt,
                                size: 30,
                                color: Colors.blue[900],
                              ),
                              onPressed: () {
                                Flushbar(
                                  isDismissible: false,
                                  blockBackgroundInteraction: true,
                                  borderRadius: 100,
                                  flushbarStyle: FlushbarStyle.GROUNDED,
                                  flushbarPosition: FlushbarPosition.TOP,
                                  titleText: Text(
                                    'Sin conexión a internet',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 18,
                                      fontWeight: FontWeight.w800,
                                    ),
                                  ),
                                  messageText: Text(
                                    'Necesitas tener conexión a internet para cerrar sesión',
                                    textAlign: TextAlign.justify,
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                  duration: Duration(milliseconds: 1500),
                                )..show(context);
                              },
                            ))
                        : Positioned(
                            right: 0,
                            top: 0,
                            child: IconButton(
                              icon: Icon(
                                FontAwesomeIcons.signOutAlt,
                                size: 30,
                                color: Colors.blue[900],
                              ),
                              onPressed: () {
                                if (Provider.of<LoginState>(context,
                                        listen: false)
                                    .isLoggedByEmail()) {
                                  Provider.of<LoginState>(context,
                                          listen: false)
                                      .logOutWithEmailAndPassword();
                                } else {
                                  Provider.of<LoginState>(context,
                                          listen: false)
                                      .logOutWithGoogle();
                                }
                              },
                            )),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: heightPhone * 0.05,
            ),
            Bookings(),
          ],
        ),
      ),
    );
  }
}

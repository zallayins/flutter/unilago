import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:unilago_prototype/pages/UserProfile/shoppingTile.dart';
import 'package:unilago_prototype/states/login_state.dart';

class ListShoppingProfile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final widthPhone = MediaQuery.of(context).size.width;
    var _query = Firestore.instance
        .collection('usuarios')
        .document(
            Provider.of<LoginState>(context, listen: false).currentUser().uid)
        .collection('compras')
        .orderBy('fecha', descending: true);

    int count = -1;
    if (_query.snapshots() != null) {
      return StreamBuilder<QuerySnapshot>(
          stream: _query.snapshots(),
          // ignore: missing_return
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting)
              return Align(
                child: Text('Cargando'),
                alignment: Alignment.center,
              );
            if (snapshot.connectionState == ConnectionState.done ||
                snapshot.connectionState == ConnectionState.active) {
              if (snapshot.data.documents.length > 0 && snapshot.hasData) {
                var shoppingList =
                    snapshot.data.documents.map((e) => e['items']).toList();
                return ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemCount: shoppingList.length > 5 ? 6 : shoppingList.length,
                  itemBuilder: (context, index) {
                    var sizeList = shoppingList[index].length;
                    if (count < sizeList) count++;
                    if (count == sizeList) count = 0;
                    var shopping = shoppingList[index][count];
                    return Container(
                      width: widthPhone * 0.5,
                      child: ShoppingTile(shopping: shopping),
                    );
                  },
                );
              } else {
                return Align(
                  child: Text('No tienes compras'),
                  alignment: Alignment.center,
                );
              }
            }
            return Align(
              child: Text('No tienes compras'),
              alignment: Alignment.center,
            );
          });
    } else {
      return Align(
        alignment: Alignment.center,
        child: Text('No tienes compras'),
      );
    }
  }
}

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class ShoppingTile extends StatelessWidget {
  final Map<String, dynamic> shopping;

  const ShoppingTile({Key key, this.shopping}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final heightPhone = MediaQuery.of(context).size.height;

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: GridTile(
        header: Container(
          height: heightPhone > 700 ? heightPhone * 0.18 : heightPhone * 0.175,
          decoration: BoxDecoration(
            color: Colors.black45,
            borderRadius: BorderRadius.all(
              Radius.circular(20),
            ),
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(
                  left: 15.0,
                  top: 10.0,
                ),
                child: Text(
                  shopping['numeral'].toString(),
                  style: TextStyle(
                    color: Colors.blue[100],
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ],
          ),
        ),
        footer: Column(
          children: <Widget>[
            ListTile(
              title: Text(
                shopping['producto'].toString(),
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w700,
                ),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
              subtitle: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    '\$ ${shopping['precio'].toString()}',
                    style: TextStyle(
                      color: Colors.grey[200],
                      fontSize: 18,
                    ),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'X',
                        style: TextStyle(
                          color: Colors.red[200],
                          fontWeight: FontWeight.w700,
                          fontSize: 10,
                        ),
                      ),
                      Text(
                        shopping['cantidad'].toString(),
                        style: TextStyle(
                          color: Colors.red[200],
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
        child: Container(
            height: heightPhone * 0.15,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(
                Radius.circular(20),
              ),
            ),
            child: CachedNetworkImage(
              imageUrl: shopping['imagen'].toString(),
              fit: BoxFit.contain,
              placeholder: (context, url) =>
                  Center(child: CircularProgressIndicator()),
              errorWidget: (context, url, error) => Icon(Icons.error),
            )),
      ),
    );
  }
}

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:unilago_prototype/shared/animator.dart';
import 'package:unilago_prototype/shared/loading.dart';
import 'package:unilago_prototype/states/login_state.dart';

class PurchasesList extends StatefulWidget {
  const PurchasesList({Key key}) : super(key: key);

  @override
  _PurchasesList createState() => _PurchasesList();
}

class _PurchasesList extends State<PurchasesList> {
  @override
  Widget build(BuildContext context) {
    final heightPhone = MediaQuery.of(context).size.height;

    var query = Firestore.instance
        .collection('usuarios')
        .document(
            Provider.of<LoginState>(context, listen: false).currentUser().uid)
        .collection('compras')
        .orderBy('fecha', descending: true)
        .snapshots();
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Colors.grey[600],
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          elevation: 0,
          backgroundColor: Theme.of(context).canvasColor,
          title: Text(
            'Compras',
            style: TextStyle(
              color: Colors.grey[600],
              fontSize: 20,
              fontWeight: FontWeight.w400,
            ),
          ),
          centerTitle: true,
        ),
        body: StreamBuilder<QuerySnapshot>(
            stream: query,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting ||
                  !snapshot.hasData ||
                  snapshot.connectionState == ConnectionState.none) {
                return Loading();
              }
              var listItems = snapshot.data.documents;
              return listItems.length > 0
                  ? ListView.builder(
                      itemCount: listItems.length,
                      itemBuilder: (BuildContext context, int index) {
                        var item = listItems[index];
                        return WidgetAnimator(
                          Card(
                            color: Colors.white,
                            margin: EdgeInsets.all(10),
                            elevation: 5,
                            child: ExpansionTile(
                              leading: Image.asset(
                                'assets/images/ok_purchase.png',
                                fit: BoxFit.contain,
                              ),
                              title: Text(
                                _getDate(item['fecha']),
                                style: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.w600),
                              ),
                              subtitle: Text(
                                  'Valor: \$ ${item['total'].toStringAsFixed(2)}'),
                              trailing: Icon(
                                Icons.keyboard_arrow_down,
                                color: Colors.grey[400],
                              ),
                              children: <Widget>[
                                _getProductsWidgets(item['items'], heightPhone),
                              ],
                            ),
                          ),
                        );
                      },
                    )
                  : Container(
                      color: Colors.white,
                      child: Center(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Image.asset('assets/images/ok_purchase.png'),
                          Text(
                            'No tienes compras',
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 20,
                              color: Colors.black87,
                            ),
                          ),
                        ],
                      )),
                    );
            }));
  }

  _getDate(date) {
    initializeDateFormatting('es_CO', null);
    return DateFormat.yMMMMd('es_CO').format(date.toDate());
  }

  Widget _getProductsWidgets(products, heightPhone) {
    List<Widget> list = List<Widget>();
    for (var i = 0; i < products.length; i++) {
      var product = products[i];
      list.add(
        Container(
          height: heightPhone > 700 ? heightPhone * 0.18 : heightPhone * 0.175,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: GridTile(
              header: Container(
                height: heightPhone > 700
                    ? heightPhone * 0.16
                    : heightPhone * 0.175,
                decoration: BoxDecoration(
                  color: Colors.black45,
                  borderRadius: BorderRadius.all(
                    Radius.circular(25),
                  ),
                ),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(
                        left: 15.0,
                        top: 10.0,
                      ),
                      child: Text(
                        product['numeral'].toString(),
                        style: TextStyle(
                          color: Colors.blue[100],
                          fontSize: 22,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              footer: Column(
                children: <Widget>[
                  ListTile(
                    title: Text(
                      product['producto'].toString(),
                      style: TextStyle(
                        fontSize: 18,
                        color: Colors.white,
                        fontWeight: FontWeight.w700,
                      ),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                    subtitle: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          '\$ ${product['precio'].toString()}',
                          style: TextStyle(
                            color: Colors.grey[200],
                            fontSize: 20,
                          ),
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              'X',
                              style: TextStyle(
                                color: Colors.red[200],
                                fontWeight: FontWeight.w700,
                                fontSize: 20,
                              ),
                            ),
                            Text(
                              product['cantidad'].toString(),
                              style: TextStyle(
                                color: Colors.red[200],
                                fontWeight: FontWeight.bold,
                                fontSize: 29,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              child: Row(
                children: <Widget>[
                  Spacer(
                    flex: 3,
                  ),
                  Container(
                    height: heightPhone * 0.13,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(
                        Radius.circular(20),
                      ),
                    ),
                    child: CachedNetworkImage(
                      imageUrl: product['imagen'],
                      fit: BoxFit.contain,
                      placeholder: (context, url) =>
                          Center(child: CircularProgressIndicator()),
                      errorWidget: (context, url, error) => Icon(Icons.error),
                    ),
                  ),
                  Spacer(
                    flex: 1,
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    }
    return Column(children: list);
  }
}

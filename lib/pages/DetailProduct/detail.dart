import 'package:badges/badges.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_3d_obj/flutter_3d_obj.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:provider/provider.dart';
import 'package:unilago_prototype/pages/DetailProduct/storeTile.dart';
import 'package:unilago_prototype/shared/animator.dart';
import 'package:unilago_prototype/states/cart_state.dart';
import 'package:unilago_prototype/states/conectivity_services.dart';
import 'package:url_launcher/url_launcher.dart';

class DetailProduct extends StatefulWidget {
  final DocumentSnapshot data;

  const DetailProduct({Key key, this.data}) : super(key: key);

  @override
  _DetailProductState createState() => _DetailProductState();
}

class _DetailProductState extends State<DetailProduct> {
  bool _selectedPrices = true;
  bool _view3d = false;

  @override
  Widget build(BuildContext context) {
    var item = widget.data;

    final widthPhone = MediaQuery.of(context).size.width;
    final heightPhone = MediaQuery.of(context).size.height;

    var connectionStatus =
        Provider.of<ConnectivityStatus>(context, listen: false);
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.grey[600],
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        elevation: 0,
        backgroundColor: Colors.white,
        title: Text(
          item['nombre'],
          style: TextStyle(
            color: Colors.grey[600],
            fontSize: 20,
            fontWeight: FontWeight.w400,
          ),
        ),
        centerTitle: true,
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 0, right: 14.0),
            child: Badge(
              showBadge:
                  Provider.of<CartState>(context).getNumberItemsCart() != 0,
              position: BadgePosition.topRight(top: 6.0),
              alignment: Alignment.center,
              animationType: BadgeAnimationType.scale,
              animationDuration: Duration(milliseconds: 1700),
              badgeContent: Text(
                Provider.of<CartState>(context).getNumberItemsCart().toString(),
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 10,
                ),
              ),
              child: GestureDetector(
                child: Icon(
                  Icons.shopping_cart,
                  color: Colors.deepPurpleAccent,
                  size: 26,
                ),
                onTap: () {
                  Navigator.of(context).pushNamed('/shopping_cart');
                },
              ),
            ),
          ),
        ],
      ),
      body: OfflineBuilder(
        connectivityBuilder: (
          BuildContext context,
          ConnectivityResult connectivity,
          Widget child,
        ) {
          final bool connected = connectivity != ConnectivityResult.none;
          return new Stack(
            fit: StackFit.expand,
            children: [
              Center(child: child),
              Positioned(
                height: 24.0,
                left: 0.0,
                right: 0.0,
                child: connected
                    ? Container()
                    : Container(
                        color: connected
                            ? Color(0xFF00EE44)
                            : Theme.of(context).buttonColor,
                        child: Center(
                          child: Text(
                            "${connected ? 'ONLINE' : 'Sin conexión a internet'}",
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
              ),
            ],
          );
        },
        child: SingleChildScrollView(
          child: Column(children: <Widget>[
            Stack(
              alignment: Alignment.topCenter,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(30),
                      bottomRight: Radius.circular(30),
                    ),
                    boxShadow: [
                      BoxShadow(
                        offset: Offset(0, 2),
                        color: Colors.black26,
                        blurRadius: 6,
                      ),
                    ],
                  ),
                  child: !_view3d
                      ? Hero(
                          transitionOnUserGestures: true,
                          tag: item['nombre'],
                          child: Container(
                            height: heightPhone < 700
                                ? heightPhone * 0.35
                                : heightPhone * 0.4,
                            width: widthPhone,
                            clipBehavior: Clip.antiAliasWithSaveLayer,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(30),
                                bottomRight: Radius.circular(30),
                              ),
                            ),
                            child: item['imagen'].toString().isNotEmpty
                                ? CachedNetworkImage(
                                    imageUrl: item['imagen'].toString(),
                                    fit: BoxFit.contain,
                                    placeholder: (context, url) => Center(
                                        child: CircularProgressIndicator()),
                                    errorWidget: (context, url, error) =>
                                        Icon(Icons.error),
                                  )
                                : Image.asset(
                                    'assets/images/noCanvas.png',
                                    fit: BoxFit.contain,
                                  ),
                          ),
                        )
                      : Column(
                          children: <Widget>[
                            Object3D(
                              size: Size(
                                widthPhone,
                                heightPhone < 700
                                    ? heightPhone * 0.32
                                    : heightPhone * 0.38,
                              ),
                              path: _selectPathModel(
                                  item['categoria'].toString()),
                              asset: true,
                              zoom: _selectZoomModel(
                                  item['categoria'].toString(), heightPhone),
                            ),
                            SizedBox(
                              height: heightPhone * 0.02,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  'Modelo hecho por: ',
                                  style: TextStyle(
                                    fontSize: 12,
                                  ),
                                ),
                                GestureDetector(
                                    child: Text(
                                      'Poly by Google',
                                      style: TextStyle(
                                        color: Theme.of(context).primaryColor,
                                        fontWeight: FontWeight.w600,
                                        fontSize: 12,
                                      ),
                                    ),
                                    onTap: () {
                                      if (connectionStatus !=
                                          ConnectivityStatus.Offline) {
                                        FutureBuilder<void>(
                                            future: _launchInBrowser(
                                                'https://poly.google.com/user/4aEd8rQgKu2'),
                                            builder: _launchStatus);
                                      } else {
                                        Flushbar(
                                          isDismissible: false,
                                          blockBackgroundInteraction: true,
                                          borderRadius: 100,
                                          flushbarStyle: FlushbarStyle.GROUNDED,
                                          flushbarPosition:
                                              FlushbarPosition.TOP,
                                          titleText: Text(
                                            'Sin conexión a internet',
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 18,
                                              fontWeight: FontWeight.w800,
                                            ),
                                          ),
                                          messageText: Text(
                                            'Necesitas internet para acceder al perfil del creador de los modelos',
                                            textAlign: TextAlign.justify,
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 16,
                                              fontWeight: FontWeight.w500,
                                            ),
                                          ),
                                          duration:
                                              Duration(milliseconds: 1500),
                                        )..show(context);
                                      }
                                    })
                              ],
                            ),
                            SizedBox(
                              height: heightPhone * 0.01,
                            ),
                          ],
                        ),
                ),
                Positioned(
                  right: 20,
                  bottom: 20,
                  child: InkWell(
                    onTap: () {
                      setState(() {
                        _view3d = !_view3d;
                      });
                    },
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10.0),
                      child: Container(
                        child: Image.asset('assets/images/3d.png'),
                        height: 40,
                        width: 40,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(30),
                  bottomRight: Radius.circular(30),
                ),
              ),
              height: heightPhone * 0.1,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  InkWell(
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    child: Text(
                      'Precios',
                      style: _selectedPrices
                          ? TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18,
                              letterSpacing: 1,
                            )
                          : TextStyle(letterSpacing: 1, height: 2),
                    ),
                    onTap: () {
                      setState(() {
                        _selectedPrices = true;
                      });
                    },
                  ),
                  //Divider the 2 texts
                  Container(
                    height: heightPhone * 0.025,
                    width: 0.3,
                    color: Colors.black,
                  ),
                  InkWell(
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    child: Align(
                      alignment: Alignment.center,
                      child: Text(
                        'Especificaciones',
                        style: !_selectedPrices
                            ? TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 18,
                                letterSpacing: 1,
                              )
                            : TextStyle(
                                letterSpacing: 1,
                              ),
                      ),
                    ),
                    onTap: () {
                      setState(() {
                        _selectedPrices = false;
                      });
                    },
                  )
                ],
              ),
            ),
            _selectedPrices
                ? WidgetAnimator(
                    Padding(
                      padding: EdgeInsets.only(top: 0, bottom: 0),
                      child: ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: item['tiendas'].length,
                        itemBuilder: (context, index) {
                          var store = item['tiendas'][index];

                          var _query2 = Firestore.instance
                              .collection('tienda')
                              .document(store['tienda'].toString());
                          return _query2.snapshots() != null &&
                                  _query2 != null &&
                                  store['tienda'].toString() != 'null' &&
                                  store['precio'] != 0
                              ? StreamBuilder<DocumentSnapshot>(
                                  stream: _query2.snapshots(),
                                  builder: (context, snapshot) {
                                    if (snapshot.hasData) {
                                      return StoreTile(
                                          data: snapshot.data,
                                          store: store,
                                          item: item);
                                    } else {
                                      return Container();
                                    }
                                  })
                              : Container();
                        },
                      ),
                    ),
                  )
                : Container(
                    height: heightPhone * 0.4,
                    child: Column(
                      children: <Widget>[
                        Spacer(
                          flex: 1,
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: widthPhone * 0.1),
                          child: Text(
                            item['descripcion'].toString().isEmpty ||
                                    item['descripcion']
                                        .toString()
                                        .contains('null')
                                ? 'Frecuencia base y max: 2.9 Ghz y 4.1 Ghz \n\n'
                                    'Núcleos: 6\n\n'
                                    'Factor de forma: 1151\n\n'
                                    '9MB Intel Smart Cache\n\n'
                                    'Soporta Intel Optane Memory\n\n'
                                    'Incluye disipador intel - CPU Cooler'
                                : item['descripcion'].toString(),
                            maxLines: 12,
                            overflow: TextOverflow.fade,
                            style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 16,
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(),
                          flex: 2,
                        ),
                      ],
                    ),
                  ),
          ]),
        ),
      ),
    );
  }

  _selectPathModel(category) {
    switch (category) {
      case 'Mouse':
        return "assets/models/ComputerMouse.obj";
      case 'Monitor':
        return "assets/models/Monitor.obj";
      case 'Teclado':
        return "assets/models/Keyboard.obj";
      default:
        return "assets/models/ComputerMouse.obj";
    }
  }

  double _selectZoomModel(category, heightPhone) {
    switch (category) {
      case 'Mouse':
        return heightPhone < 700 ? 2.0 : 2.0;
      case 'Monitor':
        return heightPhone < 700 ? 1.5 : 1.5;
      case 'Teclado':
        return heightPhone < 700 ? 0.5 : 1.0;
      default:
        return heightPhone < 700 ? 2.0 : 2.0;
    }
  }

  Future<void> _launchInBrowser(String url) async {
    if (await canLaunch(url)) {
      await launch(
        url,
        forceSafariVC: false,
        forceWebView: false,
        headers: <String, String>{'my_header_key': 'my_header_value'},
      );
    } else {
      throw 'Could not launch $url';
    }
  }

  Widget _launchStatus(BuildContext context, AsyncSnapshot<void> snapshot) {
    if (snapshot.hasError) {
      return Text('Error: ${snapshot.error}');
    } else {
      return const Text('');
    }
  }
}

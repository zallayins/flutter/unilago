import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:unilago_prototype/states/LocalDatabase/item_cartDAO.dart';
import 'package:unilago_prototype/states/cart_state.dart';

class StoreTile extends StatelessWidget {
  final DocumentSnapshot data;
  final Map<String, dynamic> store;
  final DocumentSnapshot item;

  const StoreTile({Key key, this.data, this.store, this.item})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final widthPhone = MediaQuery.of(context).size.width;
    final heightPhone = MediaQuery.of(context).size.height;
    final formatCurrency = new NumberFormat.simpleCurrency(decimalDigits: 0);

    return Stack(
      children: <Widget>[
        Container(
          margin: EdgeInsets.fromLTRB(40.0, 5.0, 20.0, 5.0),
          height: 120,
          width: double.infinity,
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
              offset: Offset(0, 2),
              blurRadius: 4,
              color: Colors.black26,
            ),
          ], color: Colors.white, borderRadius: BorderRadius.circular(20.0)),
          child: Padding(
            padding: EdgeInsets.fromLTRB(
                widthPhone > 370 ? 120 : 100.0, 15.0, 20.0, 12.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      width: widthPhone > 370 ? 120.0 : 100,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            data['nombre'].toString(),
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 18.0,
                            ),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                          Text(
                            data['numeral'].toString(),
                            style: TextStyle(
                              color: Colors.blue[100],
                              fontSize: 22,
                              fontWeight: FontWeight.bold,
                            ),
                          )
                        ],
                      ),
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        store['descuento'] != null
                            ? _getTextDiscount(store)
                            : Text(
                                '${formatCurrency.format(store['precio'])}',
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 22.0,
                                ),
                              ),
                        store['descuento'] != null
                            ? Container()
                            : SizedBox(
                                height: heightPhone * 0.012,
                              ),
                        IconButton(
                          padding: EdgeInsets.all(0),
                          icon: Icon(Icons.add_shopping_cart),
                          onPressed: () {
                            ItemCartDAO newItemCart = ItemCartDAO(
                              quantity: 1,
                              image: item['imagen'],
                              numeral: data['numeral'],
                              product: item['nombre'],
                              store: data['nombre'],
                              price: int.parse(store['precio'].toString()),
                            );
                            Provider.of<CartState>(context, listen: false)
                                .addItemCart(newItemCart, context);
                            Flushbar(
                              isDismissible: false,
                              blockBackgroundInteraction: false,
                              margin: EdgeInsets.fromLTRB(widthPhone * 0.2, 0,
                                  widthPhone * 0.2, heightPhone * 0.05),
                              borderRadius: 100,
                              flushbarStyle: FlushbarStyle.FLOATING,
                              padding: EdgeInsets.all(10),
                              messageText: Text(
                                'Se ha agregado al carrito',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              duration: Duration(milliseconds: 1000),
                            )..show(context);
                          },
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        Positioned(
          left: 20.0,
          top: 10.0,
          bottom: 10.0,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(20),
            child: data['imagen'].toString().isNotEmpty
                ? CachedNetworkImage(
                    imageUrl: data['imagen'].toString(),
                    width: 110,
                    fit: BoxFit.contain,
                    placeholder: (context, url) =>
                        Center(child: CircularProgressIndicator()),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                  )
                : Image.asset(
                    'assets/images/movileStore.png',
                    width: 110,
                    fit: BoxFit.contain,
                  ),
          ),
        ),
      ],
    );
  }

  _getTextDiscount(item) {
    return Column(
      children: <Widget>[
        Text(
          '\$${(item['precio'] - (item['precio'] * item['descuento'])).toString()}',
          style: TextStyle(
            color: Colors.red[800],
            fontSize: 20.0,
            fontWeight: FontWeight.w600,
          ),
        ),
        Text(
          '\$${item['precio'].toString()}',
          style: TextStyle(
            color: Colors.black87,
            fontSize: 16.0,
            decoration: TextDecoration.lineThrough,
          ),
        ),
      ],
    );
  }
}

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:unilago_prototype/states/login_state.dart';

class LoginFormCard extends StatelessWidget {
  final GlobalKey<FormState> formKey;
  final TextEditingController emailController;
  final TextEditingController passController;

  LoginFormCard(
      {@required this.formKey, this.emailController, this.passController});

  @override
  Widget build(BuildContext context) {
    final heightPhone = MediaQuery.of(context).size.height;
    final widthPhone = MediaQuery.of(context).size.width;

    return Container(
      width: double.infinity,
      height: heightPhone < 700 ? heightPhone * 0.37 : heightPhone * 0.355,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8.0),
          boxShadow: [
            BoxShadow(
                color: Colors.black12,
                offset: Offset(0.0, 15.0),
                blurRadius: 15.0),
            BoxShadow(
                color: Colors.black12,
                offset: Offset(0.0, -10.0),
                blurRadius: 10.0),
          ]),
      child: Padding(
        padding: EdgeInsets.only(left: 18.0, right: 16.0, top: 12.0),
        child: Form(
          key: formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Ingreso",
                    style: TextStyle(
                        fontSize: 25,
                        fontFamily: "Poppins-Bold",
                        letterSpacing: .6),
                  ),
                  GestureDetector(
                    child: Text(
                      "Crear cuenta",
                      style: TextStyle(
                        color: Colors.blue,
                        fontFamily: "Poppins-Medium",
                        fontSize: 16,
                      ),
                    ),
                    onTap: () {
                      Provider.of<LoginState>(context, listen: false)
                          .createUserPage();
                    },
                  ),
                ],
              ),
              SizedBox(
                height: heightPhone * 0.03,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: SizedBox(
                  height: 40,
                  width: widthPhone * 0.8,
                  child: TextFormField(
                    controller: emailController,
                    // ignore: missing_return
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Ingresa un correo';
                      }
                    },
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(horizontal: 15),
                        labelText: "Correo",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide:
                              BorderSide(color: Colors.black, width: 20),
                        )),
                  ),
                ),
              ),
              SizedBox(
                height: heightPhone * 0.03,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: SizedBox(
                  height: 40,
                  width: widthPhone * 0.8,
                  child: TextFormField(
                    controller: passController,
                    // ignore: missing_return
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Ingresa una contraseña';
                      }
                    },
                    obscureText: true,
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(horizontal: 15),
                        labelText: "Contraseña",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                          borderSide:
                              BorderSide(color: Colors.black, width: 20),
                        )),
                  ),
                ),
              ),
              SizedBox(
                height: heightPhone * 0.05,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Text(
                    "¿Olvidaste la contraseña?",
                    style: TextStyle(
                        color: Colors.blue,
                        fontFamily: "Poppins-Medium",
                        fontSize: 14),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:provider/provider.dart';
import 'package:unilago_prototype/pages/LogIn/loginForm_card.dart';
import 'package:unilago_prototype/pages/LogIn/singInForm_card.dart';
import 'package:unilago_prototype/pages/shoppingCart/payDialog.dart';
import 'package:unilago_prototype/shared/loading.dart';
import 'package:unilago_prototype/states/conectivity_services.dart';
import 'package:unilago_prototype/states/login_state.dart';

// ignore: must_be_immutable
class LoginPage extends StatefulWidget {
  final bool cart;
  final bool firstTime;
  const LoginPage({Key key, this.cart, this.firstTime});

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  GlobalKey<FormState> _loginformKey = GlobalKey<FormState>();
  TextEditingController _loginemailController = TextEditingController();
  TextEditingController _loginpasswordController = TextEditingController();
  GlobalKey<FormState> _singinformKey = GlobalKey<FormState>();
  TextEditingController _singinemailController = TextEditingController();
  TextEditingController _singinpasswordController = TextEditingController();

  bool _selected = true;

  Widget radioButton(bool isSelected) => Container(
        width: 16.0,
        height: 16.0,
        padding: EdgeInsets.all(2.0),
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(width: 2.0, color: Colors.black)),
        child: isSelected
            ? Container(
                width: double.infinity,
                height: double.infinity,
                decoration:
                    BoxDecoration(shape: BoxShape.circle, color: Colors.black),
              )
            : Container(),
      );

  @override
  Widget build(BuildContext context) {
    final heightPhone = MediaQuery.of(context).size.height;
    final widthPhone = MediaQuery.of(context).size.width;

    return OfflineBuilder(
      connectivityBuilder: (
        BuildContext context,
        ConnectivityResult connectivity,
        Widget child,
      ) {
        final bool connected = connectivity != ConnectivityResult.none;
        return new Stack(
          fit: StackFit.expand,
          children: [
            Center(child: child),
            Positioned(
              height: 24.0,
              bottom: kBottomNavigationBarHeight - kToolbarHeight,
              left: 0.0,
              right: 0.0,
              child: connected
                  ? Container()
                  : Container(
                      color: connected
                          ? Color(0xFF00EE44)
                          : Theme.of(context).primaryColor,
                      child: Center(
                        child: Text(
                          "${connected ? 'ONLINE' : 'Sin conexión a internet'}",
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
            ),
          ],
        );
      },
      child: Consumer(
        builder: (context, LoginState state, child) {
          if (state.isLoading()) {
            return Loading();
          } else {
            return Material(
              child: Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Align(
                        alignment: Alignment.centerRight,
                        child: Image.asset(
                          "assets/images/ai.png",
                          fit: BoxFit.contain,
                          height: heightPhone * 0.3,
                        ),
                      ),
                      Expanded(
                        child: Container(
                          width: widthPhone,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              SizedBox(
                                height: heightPhone * 0.045,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  SingleChildScrollView(
                    child: Padding(
                      padding: EdgeInsets.only(
                          left: 28.0, right: 28.0, top: heightPhone * 0.08),
                      child: Column(
                        children: <Widget>[
                          SizedBox(
                            height: heightPhone * 0.15,
                          ),
                          Provider.of<LoginState>(context).hasError()
                              ? Text(
                                  'Datos incorrectos',
                                  style: TextStyle(
                                    color: Colors.redAccent,
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                  ),
                                )
                              : Container(),
                          Provider.of<LoginState>(context).isCreatedUserPage()
                              ? SingInFormCard(
                                  formKey: _singinformKey,
                                  emailController: _singinemailController,
                                  passController: _singinpasswordController,
                                )
                              : LoginFormCard(
                                  formKey: _loginformKey,
                                  emailController: _loginemailController,
                                  passController: _loginpasswordController,
                                ),
                          SizedBox(height: heightPhone * 0.02),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Provider.of<LoginState>(context)
                                      .isCreatedUserPage()
                                  ? Container()
                                  : Row(
                                      children: <Widget>[
                                        SizedBox(
                                          width: 12.0,
                                        ),
                                        GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              _selected = !_selected;
                                            });
                                          },
                                          child: radioButton(_selected),
                                        ),
                                        SizedBox(
                                          width: 10.0,
                                        ),
                                        GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              _selected = !_selected;
                                            });
                                          },
                                          child: Text("Recordarme",
                                              style: TextStyle(
                                                  fontSize: 13,
                                                  fontFamily:
                                                      "Poppins-Medium")),
                                        )
                                      ],
                                    ),
                              //Button with gradient
                              InkWell(
                                child: Container(
                                  width: widthPhone * 0.4,
                                  height: heightPhone * 0.07,
                                  decoration: BoxDecoration(
                                      color: Theme.of(context).primaryColor,
                                      borderRadius: BorderRadius.circular(6.0),
                                      boxShadow: [
                                        BoxShadow(
                                            color: Theme.of(context)
                                                .primaryColor
                                                .withOpacity(.3),
                                            offset: Offset(0.0, 8.0),
                                            blurRadius: 8.0)
                                      ]),
                                  child: Material(
                                    color: Colors.transparent,
                                    child: InkWell(
                                      onTap: () {
                                        var connectionStatus =
                                            Provider.of<ConnectivityStatus>(
                                                context,
                                                listen: false);
                                        if (connectionStatus ==
                                            ConnectivityStatus.Offline) {
                                          Flushbar(
                                            isDismissible: false,
                                            blockBackgroundInteraction: true,
                                            borderRadius: 100,
                                            flushbarStyle:
                                                FlushbarStyle.GROUNDED,
                                            flushbarPosition:
                                                FlushbarPosition.TOP,
                                            titleText: Text(
                                              'Sin conexión a internet',
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 18,
                                                fontWeight: FontWeight.w800,
                                              ),
                                            ),
                                            messageText: Text(
                                              'Para continuar debes estar conectado a internet',
                                              textAlign: TextAlign.justify,
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 16,
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                            duration:
                                                Duration(milliseconds: 1500),
                                          )..show(context);
                                        } else {
                                          if (Provider.of<LoginState>(context,
                                                  listen: false)
                                              .isCreatedUserPage()) {
                                            if (_singinformKey.currentState
                                                .validate()) {
                                              print(
                                                  'Voy a registrarme con email y pass');
                                              Provider.of<LoginState>(context,
                                                      listen: false)
                                                  .registerWithEmailAndPassword(
                                                _singinemailController.text,
                                                _singinpasswordController.text,
                                              );
                                              _singinpasswordController.clear();
                                              _singinemailController.clear();
                                              Provider.of<LoginState>(context,
                                                      listen: false)
                                                  .loggedByEmail();
                                            }
                                          } else {
                                            if (_loginformKey.currentState
                                                .validate()) {
                                              print(
                                                  'Voy a logearme con email y pass');
                                              Provider.of<LoginState>(context,
                                                      listen: false)
                                                  .loginWithEmailAndPassword(
                                                      _loginemailController
                                                          .text,
                                                      _loginpasswordController
                                                          .text,
                                                      _selected);
                                              _loginpasswordController.clear();
                                              _loginemailController.clear();
                                              Provider.of<LoginState>(context,
                                                      listen: false)
                                                  .loggedByEmail();
                                            }
                                          }
                                        }
                                      },
                                      child: Center(
                                        child: Text(
                                            Provider.of<LoginState>(context)
                                                    .isCreatedUserPage()
                                                ? "CREAR"
                                                : "INGRESAR",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontFamily: "Poppins-Bold",
                                                fontSize: 18,
                                                letterSpacing: 1.0)),
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                          Divider(
                            color: Colors.black,
                            height: 50,
                            indent: widthPhone * 0.25,
                            endIndent: widthPhone * 0.25,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              _signInGoogleButton(context),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  widget.cart ?? false
                      ? Positioned(
                          top: heightPhone * 0.04,
                          left: widthPhone * 0.03,
                          child: IconButton(
                            icon: Icon(
                              Icons.arrow_back,
                              color: Colors.grey[600],
                            ),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                          ),
                        )
                      : Container(),
                ],
              ),
            );
          }
        },
      ),
    );
  }

  Widget _signInGoogleButton(context) {
    ConnectivityResult status;
    return status == ConnectivityResult.none
        ? OutlineButton(
            splashColor: Colors.grey,
            onPressed: () {
              Flushbar(
                isDismissible: false,
                blockBackgroundInteraction: true,
                borderRadius: 100,
                flushbarStyle: FlushbarStyle.GROUNDED,
                flushbarPosition: FlushbarPosition.TOP,
                titleText: Text(
                  'Sin conexión a internet',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.w800,
                  ),
                ),
                messageText: Text(
                  'Para continuar debes estar conectado a internet',
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                duration: Duration(milliseconds: 1500),
              )..show(context);
            },
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
            highlightElevation: 0,
            borderSide: BorderSide(color: Colors.grey),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image(
                      image: AssetImage("assets/images/google_logo.png"),
                      height: 35.0),
                  Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: Text(
                      'Ingresa con Google',
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.grey,
                      ),
                    ),
                  )
                ],
              ),
            ),
          )
        : OutlineButton(
            splashColor: Colors.grey,
            onPressed: () {
              Provider.of<LoginState>(context, listen: false)
                  .notLoggedByEmail();
              Provider.of<LoginState>(context, listen: false).logInWithGoogle();
              if (widget.cart) {
                Future.delayed(Duration(seconds: 6)).whenComplete(() {
                  Navigator.pop(context);
                  _getPayAlertDialog(context);
                });
              }
            },
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
            highlightElevation: 0,
            borderSide: BorderSide(color: Colors.grey),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image(
                      image: AssetImage("assets/images/google_logo.png"),
                      height: 35.0),
                  Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: Text(
                      'Ingresa con Google',
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.grey,
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
  }

  _getPayAlertDialog(context) {
    return showDialog(
        context: context,
        builder: (context) {
          return PayDialog(
            user: Provider.of<LoginState>(context, listen: false).currentUser(),
          );
        });
  }
}

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:unilago_prototype/pages/LogIn/login.dart';
import 'package:unilago_prototype/pages/UserProfile/user_profile.dart';
import 'package:unilago_prototype/pages/homepage/home_page.dart';
import 'package:unilago_prototype/pages/recomendation.dart';
import 'package:unilago_prototype/states/login_state.dart';

class BottomNavBar extends StatefulWidget {
  BottomNavBar({Key key}) : super(key: key);

  @override
  _BottomNavBarState createState() => _BottomNavBarState();
}

class _BottomNavBarState extends State<BottomNavBar> {
  int _currentIndex = 1;
  static bool ft = false;

  @override
  void initState() {
    setState(() {
      ft = true;
    });
    super.initState();
  }

  final tabsNoLogged = [
    LoginPage(),
    HomePage(
      query: Firestore.instance.collection('productos').snapshots(),
    ),
    Recomendation(),
  ];
  final tabsLogged = [
    UserProfile(),
    HomePage(
      query: Firestore.instance.collection('productos').snapshots(),
    ),
    Recomendation(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: _currentIndex,
          selectedFontSize: 20,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.people),
              title: _currentIndex == 0
                  ? Container(
                      width: 4.0,
                      height: 4.0,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(
                              width: 2.0,
                              color: Theme.of(context).primaryColor)),
                    )
                  : Container(),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              title: _currentIndex == 1
                  ? Container(
                      width: 4.0,
                      height: 4.0,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(
                              width: 2.0,
                              color: Theme.of(context).primaryColor)),
                    )
                  : Container(),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.chat),
              title: _currentIndex == 2
                  ? Container(
                      width: 4.0,
                      height: 4.0,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(
                              width: 2.0,
                              color: Theme.of(context).primaryColor)),
                    )
                  : Container(),
            ),
          ],
          onTap: (index) {
            setState(() {
              _currentIndex = index;
            });
          },
        ),
        body: _decideTap(_currentIndex, context));
  }

  _decideTap(int index, BuildContext context) {
    if (Provider.of<LoginState>(context).isLogged()) {
      return tabsLogged[_currentIndex];
    } else {
      if (_currentIndex != 0) Provider.of<LoginState>(context).noError();
      return tabsNoLogged[_currentIndex];
    }
  }
}

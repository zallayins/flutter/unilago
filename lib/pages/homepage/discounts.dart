import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CarouselWithIndicator extends StatefulWidget {
  @override
  _CarouselWithIndicatorState createState() => _CarouselWithIndicatorState();
}

class _CarouselWithIndicatorState extends State<CarouselWithIndicator> {
  var _query = Firestore.instance.collection('promociones').snapshots();
  var _query2 = Firestore.instance.collection('productos');

  @override
  Widget build(BuildContext context) {
    final widthPhone = MediaQuery.of(context).size.width;
    final heightPhone = MediaQuery.of(context).size.height;

    return StreamBuilder<QuerySnapshot>(
        stream: _query,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            var listItems = snapshot.data.documents;
            return Column(
              children: <Widget>[
                Stack(overflow: Overflow.clip, children: [
                  CarouselSlider.builder(
                    scrollDirection: Axis.horizontal,
                    aspectRatio: 13 / 8,
                    enableInfiniteScroll: false,
                    initialPage: (listItems.length / 2).floor() ?? 0,
                    itemCount: listItems.length,
                    itemBuilder: (context, index) {
                      var item = listItems[index].data;
                      return Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            height: heightPhone * 0.26,
                            width: MediaQuery.of(context).size.width,
                            margin: EdgeInsets.symmetric(horizontal: 10.0),
                            decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                  offset: Offset(0, 0),
                                  blurRadius: 4,
                                  color: Colors.black54,
                                )
                              ],
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(20),
                            ),
                            child: StreamBuilder<DocumentSnapshot>(
                                stream: _query2
                                    .document(item['numero'].toString())
                                    .snapshots(),
                                builder: (context, snapshot) {
                                  // ignore: missing_return
                                  var detail = snapshot.data;
                                  if (item['precio'] != 0 &&
                                      item['producto'] != "") {
                                    return GestureDetector(
                                      onTap: () {
                                        Navigator.of(context).pushNamed(
                                            '/detail',
                                            arguments: detail);
                                      },
                                      child: Stack(
                                        overflow: Overflow.clip,
                                        alignment: Alignment.topRight,
                                        children: <Widget>[
                                          ClipRRect(
                                            child: CachedNetworkImage(
                                              imageUrl:
                                                  item['imagen'].toString(),
                                              height: heightPhone * 0.3,
                                              width: widthPhone * 0.5,
                                              fit: BoxFit.contain,
                                              placeholder: (context, url) => Center(
                                                  child:
                                                      CircularProgressIndicator()),
                                              errorWidget:
                                                  (context, url, error) =>
                                                      Icon(Icons.error),
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(30),
                                            clipBehavior:
                                                Clip.antiAliasWithSaveLayer,
                                          ),
                                          Container(
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(20),
                                              gradient: LinearGradient(
                                                colors: [
                                                  Colors.transparent,
                                                  Colors.white54,
                                                ],
                                                end: Alignment.bottomCenter,
                                              ),
                                            ),
                                          ),
                                          Positioned.fill(
                                            left: widthPhone * 0.05,
                                            bottom: heightPhone * 0.02,
                                            top: heightPhone * 0.02,
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: <Widget>[
                                                Text(
                                                  item['numeral'],
                                                  style: TextStyle(
                                                    color: Colors.blue[100],
                                                    fontSize: 22,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                                Spacer(),
                                                Text(
                                                  item['producto'],
                                                  style: TextStyle(
                                                    color: Colors.black,
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 20.0,
                                                  ),
                                                  maxLines: 1,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ],
                                            ),
                                          ),
                                          Positioned(
                                            left: widthPhone * 0.05,
                                            bottom: heightPhone * 0.09,
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text(
                                                  '\$${(item['precio'] - (item['precio'] * item['descuento'])).toString()}',
                                                  style: TextStyle(
                                                    color: Colors.red[800],
                                                    fontSize: 20.0,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                                SizedBox(
                                                  height: 4.0,
                                                ),
                                                Text(
                                                  '\$${item['precio'].toString()}',
                                                  style: TextStyle(
                                                    color: Colors.black87,
                                                    fontSize: 16.0,
                                                    decoration: TextDecoration
                                                        .lineThrough,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Positioned(
                                              right: widthPhone * 0.022,
                                              top: widthPhone * 0.022,
                                              child: Text(
                                                '- ${((item['descuento'] * 100).toInt()).toString()}%',
                                                style: TextStyle(
                                                  fontWeight: FontWeight.w700,
                                                  fontSize: 30,
                                                  color: Colors.grey[800],
                                                ),
                                              )),
                                        ],
                                      ),
                                    );
                                  } else {
                                    return Container();
                                  }
                                }),
                          ),
                        ],
                      );
                    },
                  ),
                ]),
              ],
            );
          } else {
            return Align(
              child: Text('Cargando'),
            );
          }
        });
  }
}

//External Libraries
import 'package:badges/badges.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:provider/provider.dart';
import 'package:unilago_prototype/pages/homepage/Filters/listFilters.dart';
import 'package:unilago_prototype/pages/homepage/products_list.dart';
import 'package:unilago_prototype/shared/animator.dart';
import 'package:unilago_prototype/shared/loading.dart';
import 'package:unilago_prototype/states/cart_state.dart';
import 'package:unilago_prototype/states/products_state.dart';

import 'discounts.dart';

// ignore: must_be_immutable
class HomePage extends StatefulWidget {
  bool firstTime;
  Stream<QuerySnapshot> query;
  HomePage({Key key, this.query, this.firstTime}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
    widget.firstTime = true;
    widget.query = Firestore.instance.collection('productos').snapshots();
  }

  @override
  Widget build(BuildContext context) {
    final widthPhone = MediaQuery.of(context).size.width;
    final theme = Theme.of(context);
    widget.query = Provider.of<ProductState>(context).getInitialQuery();

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).canvasColor,
        elevation: 0.0,
        centerTitle: false,
        titleSpacing: widthPhone * 0.05,
        title: TextFormField(
          decoration: InputDecoration(
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30),
              borderSide: BorderSide(
                color: Colors.indigo,
                width: 20,
              ),
            ),
            contentPadding: EdgeInsets.all(2),
            hintText: "Buscar producto",
            prefixIcon: IconButton(
              icon: Icon(Icons.search),
              onPressed: () {},
            ),
          ),
        ),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 0.0, right: 14.0),
            child: Badge(
              showBadge: Provider.of<CartState>(context).getNumberItemsCart() !=
                      null
                  ? Provider.of<CartState>(context).getNumberItemsCart() != 0
                  : false,
              position: BadgePosition.topRight(top: 6.0),
              alignment: Alignment.center,
              animationType: BadgeAnimationType.scale,
              animationDuration: Duration(seconds: 1),
              badgeContent: Text(
                Provider.of<CartState>(context).getNumberItemsCart() != null
                    ? Provider.of<CartState>(context)
                        .getNumberItemsCart()
                        .toString()
                    : '',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 10,
                ),
              ),
              child: GestureDetector(
                child: Icon(
                  Icons.shopping_cart,
                  color: Colors.deepPurpleAccent,
                  size: 26,
                ),
                onTap: () {
                  Navigator.of(context).pushNamed('/shopping_cart');
                },
              ),
            ),
          ),
        ],
      ),
      body: OfflineBuilder(
        connectivityBuilder: (
          BuildContext context,
          ConnectivityResult connectivity,
          Widget child,
        ) {
          final bool connected = connectivity != ConnectivityResult.none;
          return new Stack(
            fit: StackFit.expand,
            children: [
              Center(child: child),
              Positioned(
                bottom: kBottomNavigationBarHeight - kToolbarHeight,
                height: 24.0,
                left: 0.0,
                right: 0.0,
                child: connected
                    ? Container()
                    : Container(
                        color:
                            connected ? Color(0xFF00EE44) : theme.primaryColor,
                        child: Center(
                          child: Text(
                            "${connected ? 'ONLINE' : 'Sin conexión a internet'}",
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
              ),
            ],
          );
        },
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: WidgetAnimator(
            Column(
              children: <Widget>[
                CarouselWithIndicator(),
                ListFilters(),
                StreamBuilder<QuerySnapshot>(
                    stream: widget.query,
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting ||
                          !snapshot.hasData ||
                          snapshot.connectionState == ConnectionState.none) {
                        return Loading();
                      }
                      return Product(
                        documents: snapshot.data.documents,
                        means: [],
                      );
                    }),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

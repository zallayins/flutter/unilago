import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:unilago_prototype/states/products_state.dart';

class ListFilters extends StatefulWidget {
  @override
  _ListFiltersState createState() => _ListFiltersState();
}

class _ListFiltersState extends State<ListFilters> {
  var _query = Firestore.instance.collection('categorias').snapshots();

  @override
  Widget build(BuildContext context) {
    final widthPhone = MediaQuery.of(context).size.width;
    final heightPhone = MediaQuery.of(context).size.height;

    return SizedBox(
      height: heightPhone * 0.14,
      child: StreamBuilder<QuerySnapshot>(
          stream: _query,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting ||
                snapshot.connectionState == ConnectionState.none)
              return Align(
                child: Text('Cargando'),
                alignment: Alignment.center,
              );
            var list =
                List.generate(snapshot.data.documents.length, (index) => false);
            Provider.of<ProductState>(context).addFilterSelectList(list);
            return ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: snapshot.data.documents.length,
                itemBuilder: (BuildContext context, int index) {
                  var category = snapshot.data.documents[index];

                  return GestureDetector(
                    onTap: () {
                      Provider.of<ProductState>(context, listen: false)
                          .changeValueSelected(index);
                      Provider.of<ProductState>(context, listen: false)
                          .setCurrentQuery(category['nombre'].toString());
                      var sendCategory = category['nombre'].toString();

                      Navigator.of(context).pushNamed('/listItemsFiltered',
                          arguments: sendCategory);
                    },
                    child: Padding(
                      padding: EdgeInsets.only(
                        left: widthPhone * 0.035,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            height: 54,
                            width: 54,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30),
                              border: Provider.of<ProductState>(context,
                                          listen: false)
                                      .getListSelectedFilters()[index]
                                  ? Border.all(
                                      width: 1.0,
                                      color: Color(0xff6B3FFB),
                                    )
                                  : Border.all(
                                      width: 1.0,
                                      color: Colors.grey[400],
                                    ),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Image.asset(
                                'assets/images/${category['nombre'].toString()}.png',
                                fit: BoxFit.contain,
                              ),
                            ),
                          ),
                          Container(
                            width: 70,
                            child: Text(
                              category['nombre'].toString(),
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 11),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                });
          }),
    );
  }
}

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:unilago_prototype/shared/loading.dart';
import 'package:unilago_prototype/states/products_state.dart';

import '../../../shared/animator.dart';

class ListItemsFiltered extends StatefulWidget {
  final String category;

  const ListItemsFiltered({Key key, this.category}) : super(key: key);

  @override
  _ListItemsFilteredState createState() => _ListItemsFilteredState();
}

class _ListItemsFilteredState extends State<ListItemsFiltered> {
  @override
  Widget build(BuildContext context) {
    var query =
        Provider.of<ProductState>(context, listen: false).getCurrentQuery();

    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.grey[600],
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        elevation: 0,
        backgroundColor: Theme.of(context).canvasColor,
        title: Text(
          widget.category ?? 'Categoría',
          style: TextStyle(
            color: Colors.grey[600],
            fontSize: 20,
            fontWeight: FontWeight.w400,
          ),
        ),
      ),
      body: StreamBuilder<QuerySnapshot>(
          stream: query,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting ||
                !snapshot.hasData ||
                snapshot.connectionState == ConnectionState.none) {
              return Loading();
            }
            var listItems = snapshot.data.documents;
            return ListView.builder(
              itemCount: listItems.length,
              itemBuilder: (BuildContext context, int index) {
                var item = listItems[index];
                return WidgetAnimator(
                  Container(
                    height: 120,
                    child: Card(
                      color: Colors.white,
                      margin: EdgeInsets.all(10),
                      elevation: 5,
                      child: ListTile(
                        contentPadding: EdgeInsets.all(8.0),
                        leading: item['imagen'].toString().isNotEmpty
                            ? CachedNetworkImage(
                                imageUrl: item['imagen'].toString(),
                                fit: BoxFit.contain,
                                placeholder: (context, url) =>
                                    Center(child: CircularProgressIndicator()),
                                errorWidget: (context, url, error) =>
                                    Icon(Icons.error),
                              )
                            : Image.asset(
                                'assets/images/noCanvas.png',
                                fit: BoxFit.contain,
                              ),
                        title: Text(
                          item['nombre'],
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.w600),
                        ),
                        subtitle: Text(''),
                        trailing: Icon(
                          Icons.arrow_forward_ios,
                          color: Colors.grey[400],
                        ),
                        onTap: () {
                          Navigator.of(context)
                              .pushNamed('/detail', arguments: item);
                        },
                      ),
                    ),
                  ),
                );
              },
            );
          }),
    );
  }
}

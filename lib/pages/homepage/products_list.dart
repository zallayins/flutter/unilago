import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class Product extends StatefulWidget {
  final List<DocumentSnapshot> documents;
  final List<double> means;

  Product({
    Key key,
    this.documents,
    this.means,
  }) {
    _calculatedMean(documents);
  }

  //Actions that can be initialize final attributes

  void _calculatedMean(documents) {
    double mean = 0;
    for (var product in documents) {
      product['tiendas'].forEach((seller) {
        mean += double.parse(seller['precio'].toString());
      });
      mean = mean / product['tiendas'].length;
      means.add(mean);
    }
  }

  @override
  _ProductState createState() => _ProductState();
}

class _ProductState extends State<Product> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(
              left: MediaQuery.of(context).size.width * 0.05, bottom: 10.0),
          child: Text(
            'Agregados recientemente',
            style: TextStyle(
              fontSize: 18.0,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
        _buildPagesFiltered(context),
      ],
    );
  }

  Widget _buildPagesFiltered(context) {
    final formatCurrency = new NumberFormat.simpleCurrency(
      decimalDigits: 2,
    );
    final listItems = _validateProducts(widget.documents);
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 9.0),
      child: GridView.builder(
          physics: NeverScrollableScrollPhysics(),
          addAutomaticKeepAlives: true,
          shrinkWrap: true,
          scrollDirection: Axis.vertical,
          itemCount: listItems.length,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            mainAxisSpacing: 3.0,
            crossAxisSpacing: 3.0,
          ),
          itemBuilder: (BuildContext context, int index) {
            DocumentSnapshot item = listItems[index];
            if (item['nombre'] == "" || item['tiendas'].length == 0)
              return Container(
                height: 0,
                width: 0,
              );
            else {
              return Card(
                color: Colors.white,
                margin: EdgeInsets.all(8),
                child: Hero(
                  transitionOnUserGestures: true,
                  tag: item['nombre'],
                  child: Material(
                    color: Colors.white,
                    child: InkWell(
                      onTap: () {
                        Navigator.of(context)
                            .pushNamed('/detail', arguments: item);
                      },
                      child: GridTile(
                        footer: Container(
                          decoration: BoxDecoration(
                            color: Colors.black45,
                            borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(20),
                              bottomRight: Radius.circular(20),
                            ),
                          ),
                          child: ListTile(
                            title: Text(
                              item['nombre'].toString(),
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                              ),
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                            ),
                            subtitle: Text(
                              '${formatCurrency.format(widget.means[index])}',
                              style: TextStyle(
                                color: Colors.grey[200],
                              ),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ),
                        child: item['imagen'].toString().isNotEmpty
                            ? CachedNetworkImage(
                                imageUrl: item['imagen'].toString(),
                                fit: BoxFit.contain,
                                placeholder: (context, url) =>
                                    Center(child: CircularProgressIndicator()),
                                errorWidget: (context, url, error) =>
                                    Icon(Icons.error),
                              )
                            : Image.asset(
                                'assets/images/noCanvas.png',
                                fit: BoxFit.contain,
                              ),
                      ),
                    ),
                  ),
                ),
                elevation: 4.0,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                clipBehavior: Clip.antiAliasWithSaveLayer,
              );
            }
          }),
    );
  }

  _validateProducts(List<DocumentSnapshot> items) {
    List listUpdated = List();
    List indexToDelete = List();
    items.forEach((DocumentSnapshot e) {
      if (e == null ||
          e['nombre'] == "" ||
          e['tiendas'].length == 0 ||
          e['categoria'] == "") {
        indexToDelete.add(e);
      }
    });
    indexToDelete.forEach((element) {
      items.remove(element);
    });

    return listUpdated = items;
  }
}
